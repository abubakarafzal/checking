from time import sleep

import allure
import pytest
from utils.Common import *
from dotenv import load_dotenv
from selenium.webdriver.common.by import By

from tests.test_base import BaseTest
from utils.users import get_json_data

load_dotenv()


@allure.story("Forgot Password Feature's Functionality")
@allure.severity(allure.severity_level.CRITICAL)
@pytest.mark.check
@allure.parent_suite("Custom parent suite")
@allure.suite("Custom suite")
@allure.sub_suite("Custom sub suite")
class TestsAnnotationFirstTestCase(BaseTest):
    user = get_json_data('users')
    annotation_name = "my annotation 1"
    model_name = "as1-oc-214.stp"
    x_point = 96
    y_point = 54

    def find_elements_by_xpath(self, xpath: str) -> list:
        return self.driver.find_elements(By.XPATH, value=xpath)

    @allure.description("valid annotation")
    @allure.title("Title_Annotation_First_Allure")
    @allure.tag("Tagged test")
    @pytest.mark.check
    @pytest.mark.run(order=1)
    def test_Annotation_First_TestCase(self):
        """
        Get the name and password from the .env file and log in.
        """

        password = self.user['valid_user']["password"]
        username = self.user['valid_user']["username"]
        print("user")
        self.login_page.login(username, password)
        print("\nA1.1 Add and remove annotation on CAD model using Remove option in annotation popup menu")
        self.home_page.new_project_link_model(DATA_DIR_Cloud + self.model_name)

        # Get number of existing annotations
        # Wait until content annotation will appear
        wait_condition_located(self, content_annotations_available, 60)
        if not is_element_present(self, By.XPATH, list_annotations):
            print("A1.1.1 Project initially contains 0 annotations")
        else:
            raise TimeoutException(msg="A1.1.1 Project initially contains annotations!")

        # Create an annotation
        create_an_annotation(self, self.x_point, self.y_point, description=f"{self.annotation_name}")

        content_annotations_open_button(self).click()
        sleep(1)
        is_exist(self, xpath="//project-tree/div/cdk-virtual-scroll-viewport/div/div/div/div/span/span",
                 object_name=f"{self.annotation_name}",
                 positive_message="A1.1.2 The annotation was created",
                 negative_message="A1.1.2 The annotation was not created")
        # Select the annotation and delete
        delete_an_annotation(self, self.x_point, self.y_point, remove=True)

        if not is_element_present(self, By.XPATH, f"//project-tree//span[contains(., '{self.annotation_name}')]"):
            print("A1.1.3 The annotation was deleted")
        else:
            raise TimeoutException(msg="A1.1.3 The annotation was not deleted!")

        # log_parse(self)
        cleanup(self, "Unnamed")

    @allure.description("valid annotation")
    @allure.title("Title_Annotation_Second_Allure")
    @allure.tag("Tagged test")
    @pytest.mark.check
    @pytest.mark.run(order=2)
    def test_Annotation_Second_TestCase(self):
        password = self.user['valid_user']["password"]
        username = self.user['valid_user']["username"]
        print("user")
        self.login_page.login(username, password)
        print("\nA1.2 Add and remove annotation on CAD model using Remove button in Annotation tab in Content panel")
        self.home_page.use_existing_or_new_project_link_model(DATA_DIR_Cloud + self.model_name, "Unnamed")

        # Create an annotation
        create_an_annotation(self, self.x_point, self.y_point, description=f"{self.annotation_name}")
        content_annotations_open_button(self).click()
        sleep(1)
        is_exist(self, xpath="//project-tree/div/cdk-virtual-scroll-viewport/div/div/div/div/span/span",
                 object_name=f"{self.annotation_name}",
                 positive_message="A1.2.1 The annotation was created",
                 negative_message="A1.2.1 The annotation was not be created")

        # Remove annotation
        find_element_xpath(self, f"//project-tree//span[contains(., '{self.annotation_name}')]").click()
        sleep(1)
        delete_button(self).click()
        sleep(1)
        # Confirm deleting
        wait_condition_clickable(self, confirm_remove_clickable, 10)
        confirm_remove_button(self).click()
        sleep(1)

        # Check if number of annotations is equal to initial
        if not is_element_present(self, By.XPATH, f"//project-tree//span[contains(., '{self.annotation_name}')]"):
            print("A1.2.2 The annotation was deleted")
        else:
            raise TimeoutException(msg="A1.2.2 The annotation was not deleted!")

        cleanup(self, "Unnamed")

    # @allure.description("valid annotation")
    # @allure.title("Title_Annotation_Third_Allure")
    # @allure.tag("Tagged test")
    # @pytest.mark.run(order=3)
    # def test_Annotation_Third_TestCase(self):
    #     password = self.user['valid_user']["password"]
    #     username = self.user['valid_user']["username"]
    #     print("user")
    #     self.login_page.login(username, password)
    #     self.home_page.use_existing_or_new_project_link_model(DATA_DIR_Cloud + self.model_name, "Unnamed")
    #
    #     print("\nA1.3 Add an annotation on CAD model and copy text of created annotation to clipboard")
    #     create_an_annotation(self, self.x_point, self.y_point, description=f"{self.annotation_name}",
    #                          copy_to_the_clipboard=True)
    #
    #     if is_element_present(self, By.XPATH,
    #                           """//simple-snack-bar/span[contains(., "Annotation text is copied to the clipboard")]"""):
    #         print("A1.3.1 Annotation text is copied to the clipboard")
    #     else:
    #         raise TimeoutException(msg="A1.3.1 Verification message was not appears")
    #
    #     # Remove annotation
    #     find_element_xpath(self, f"//project-tree//span[contains(., '{self.annotation_name}')]").click()
    #     sleep(1)
    #     delete_button(self).click()
    #     sleep(1)
    #     # Confirm deleting
    #     wait_condition_clickable(self, confirm_remove_clickable, 10)
    #     confirm_remove_button(self).click()
    #     sleep(1)
    #
    #     # Check if number of annotations is equal to initial
    #     if not is_element_present(self, By.XPATH, f"//project-tree//span[contains(., '{self.annotation_name}')]"):
    #         print("A1.3.2 The annotation was deleted")
    #     else:
    #         raise TimeoutException(msg="A1.3.2 The annotation was not deleted!")
    #
    #     cleanup(self, "Unnamed")
    #
    # @allure.description("valid annotation")
    # @allure.title("Title_Annotation_Fourth_Allure")
    # @allure.tag("Tagged test")
    # @pytest.mark.run(order=4)
    # def test_Annotation_Fourth_TestCase(self):
    #     password = self.user['valid_user']["password"]
    #     username = self.user['valid_user']["username"]
    #     print("user")
    #     self.login_page.login(username, password)
    #     self.home_page.use_existing_or_new_project_link_model(DATA_DIR_Cloud + self.model_name, "Unnamed")
    #
    #     print("\nA1.4 Add an annotation with text from the clipboard")
    #     create_an_annotation(self, self.x_point, self.y_point, text_from_clipboard=True)
    #
    #     is_exist(self, xpath="//project-tree/div/cdk-virtual-scroll-viewport/div/div/div/div/span/span",
    #              object_name=f"{self.annotation_name}",
    #              positive_message="A1.4.1 The annotation was created",
    #              negative_message="A1.4.1 The annotation was not be created")
    #
    #     # Remove annotation
    #     find_element_xpath(self, f"//project-tree//span[contains(., '{self.annotation_name}')]").click()
    #     sleep(1)
    #     delete_button(self).click()
    #     sleep(1)
    #     # Confirm deleting
    #     wait_condition_clickable(self, confirm_remove_clickable, 10)
    #     confirm_remove_button(self).click()
    #     sleep(1)
    #
    #     # Check if number of annotations is equal to initial
    #     if not is_element_present(self, By.XPATH, f"//project-tree//span[contains(., '{self.annotation_name}')]"):
    #         print("A1.4.2 The annotation was deleted")
    #     else:
    #         raise TimeoutException(msg="A1.4.2 The annotation was not deleted!")
    #
    #     cleanup(self, "Unnamed")
    #
    # @allure.description("valid annotation")
    # @allure.title("Title_Annotation_Fifth_Allure")
    # @allure.tag("Tagged test")
    # @pytest.mark.run(order=5)
    # def test_Annotation_Fifth_TestCase(self):
    #     password = self.user['valid_user']["password"]
    #     username = self.user['valid_user']["username"]
    #     print("user")
    #     self.login_page.login(username, password)
    #     self.home_page.use_existing_or_new_project_link_model(DATA_DIR_Cloud + self.model_name, "Unnamed")
    #
    #     print("\nA1.5 Add an annotation on CAD model and remove this CAD model with created annotation")
    #     create_an_annotation(self, self.x_point, self.y_point, description=f"{self.annotation_name}")
    #
    #     is_exist(self, xpath="//project-tree/div/cdk-virtual-scroll-viewport/div/div/div/div/span/span",
    #              object_name=f"{self.annotation_name}",
    #              positive_message="A1.5.1 The annotation was created",
    #              negative_message="A1.5.1 The annotation was not be created")
    #
    #     # Remove CAD model
    #     # Wait until content CAD models menu button will appear
    #     find_element_xpath(self, f"//project-tree//span[contains(., '{self.model_name}')]").click()
    #     sleep(1)
    #     delete_button(self).click()
    #     sleep(1)
    #     # Confirm deleting
    #     wait_condition_clickable(self, confirm_remove_clickable, 10)
    #     confirm_remove_button(self).click()
    #     sleep(1)
    #
    #     # Check if number of annotations is equal to initial
    #     # Wait until content annotations menu button will appear
    #     wait_condition_located(self, content_annotations_available, 10)
    #     if not is_element_present(self, By.XPATH, f"//project-tree//span[contains(., '{self.annotation_name}')]"):
    #         print("A1.5.2 The annotation was deleted with project")
    #     else:
    #         raise TimeoutException(msg="1.5.2 Project contains annotations")
    #
    #     if is_element_present(self, By.XPATH, f"//project-tree//span[contains(., '{self.model_name}')]"):
    #         raise TimeoutException(msg="1.5.3 Model was not deleted")
    #
    #     cleanup(self, "Unnamed")
