from time import sleep

import allure
import pytest
from utils.Common import *
from dotenv import load_dotenv
from selenium.webdriver.common.by import By

from tests.test_base import BaseTest
from utils.users import *

load_dotenv()
verificationErrors = []


def find_elements_by_xpath(self, xpath: str) -> list:
    return self.driver.find_elements(By.XPATH, value=xpath)


@allure.story("Forgot Password Feature's Functionality")
@allure.severity(allure.severity_level.CRITICAL)
@pytest.mark.check
@allure.parent_suite("Custom parent suite")
@allure.suite("Custom suite")
@allure.sub_suite("Custom sub suite")
class TestsAddTagTestCase(BaseTest):
    # 'users' is the file name without .json extension
    user = get_json_data('users')
    annotation_name = "my annotation 1"
    model_name = "as1-oc-214.stp"
    x_point = 96
    y_point = 54

    def find_elements_by_xpath(self, xpath: str) -> list:
        return self.driver.find_elements(By.XPATH, value=xpath)

    @allure.description("Test TAG  annotation")
    @allure.title("Title Tag Test Calse")
    @allure.tag("Tagged test")
    @pytest.mark.tagTest
    @pytest.mark.run(order=1)
    def test_AddTagTest_First_TestCase(self):
        """
        Get the name and password from the .env file and log in.
        """

        """
            Get the name and the password from the .env file and log in.
            """
        password = self.user['valid_user']["password"]
        username = self.user['valid_user']["username"]
        print("user")
        self.login_page.login(username, password)

        print("\nJ1.1 Adding and removing tags on CAD model")
        # self.home_page.new_project_link_model(DATA_DIR_Cloud + "as1-oc-214.stp")
        self.home_page.use_existing_or_new_project_link_model(DATA_DIR_Cloud + self.model_name, "Unnamed")

        self.home_page.canvas_loaded()
        point_setter(self, 96, 54)


        wait_condition_located(self, properties_button_clickable, 15)
        properties_button(self).click()
        sleep(1)
        wait_condition_clickable(self, tags_button_clickable, 10)
        tags_button(self).click()
        sleep(2)
        n_tags_part_1 = len(tags_list(self))
        print(f"J1.1.1 Part_1 initially contains {n_tags_part_1} tags")

        # Select part_2 of model
        point_setter(self, 115, 35)

        # Get number of existing tags for part_2
        n_tags_part_2 = len(tags_list(self))
        print(f"J1.1.2 Part_2 initially contains {n_tags_part_2} tags")

        # Add one tag for part_2
        # Wait until add tags button will be available
        wait_condition_clickable(self, add_tag_clickable, 10)
        add_tag_button(self).click()
        sleep(2)
        # Wait until tag input field is available
        wait_condition_located(self, tag_input_available, 10)
        tag_input(self).send_keys("tag 0")
        sleep(2)
        # Wait until add tags ok button is available
        wait_condition_clickable(self, tag_ok_clickable, 10)
        # Click "Save" button
        tag_ok_button(self).click()
        sleep(1)
        # Check if number of tags for part_2 has increased by 1
        now_tags_part_2 = n_tags_part_2 + 1
        cur_tags_part_2 = len(tags_list(self))
        compare_values(self, "J1.1.3 Add tag part_2: ", cur_tags_part_2, now_tags_part_2)
        sleep(2)

        # Add two tags for part_1
        point_setter(self, 832, 470)
        # Wait until add tags button is available
        wait_condition_clickable(self, add_tag_clickable, 10)
        add_tag_button(self).click()
        sleep(2)
        # Wait until tag input field is available
        wait_condition_located(self, tag_input_available, 10)
        tag_input(self).send_keys("my tag 1")
        sleep(2)
        # Wait until add tags ok button is available
        wait_condition_clickable(self, tag_ok_clickable, 10)
        # Click "Save" button
        tag_ok_button(self).click()
        sleep(2)
        # Wait until add tags button is available
        wait_condition_clickable(self, add_tag_clickable, 10)
        add_tag_button(self).click()
        sleep(2)
        # Wait until tag input field is available
        wait_condition_located(self, tag_input_available, 10)
        tag_input(self).send_keys("my tag 2")
        sleep(2)
        # Wait until add tags ok button is available
        wait_condition_clickable(self, tag_ok_clickable, 10)
        tag_ok_button(self).click()
        # Click "Save" button
        sleep(1)
        # Check if number of tags for part_1 is increased by 2
        now_tags_part_1 = n_tags_part_1 + 2
        cur_tags_part_1 = len(tags_list(self))
        compare_values(self, "J1.1.4 Add tag part_1: ", cur_tags_part_1, now_tags_part_1)
        sleep(2)

        # Close dialog
        # Wait until close tags button is available
        wait_condition_clickable(self, close_tag_clickable, 10)
        close_tag_button(self).click()
        sleep(2)
        # Wait until tags button is available
        wait_condition_clickable(self, tags_button_clickable, 10)
        point_setter(self, 693, 446)

        # Open search-box
        search_term = "tag"
        for x in range(0, len(search_term)):
            search_input(self).send_keys(search_term[x])
        search_input(self).send_keys(Keys.ENTER)
        sleep(2)
        found_items = search_found_list(self)
        for item in found_items:
            print("    found: " + item.text)
        compare_values(self, "J1.1.5 Found items: ", len(found_items), 2)
        search_input(self).clear()
        search_term = "my"
        for x in range(0, len(search_term)):
            search_input(self).send_keys(search_term[x])
        search_input(self).send_keys(Keys.ENTER)
        sleep(2)
        found_items = search_found_list(self)
        for item in found_items:
            print("    found: " + item.text)
        compare_values(self, "J1.1.6 Found items: ", len(found_items), 1)
        if len(found_items) > 0:
            compare_values(self, "J1.1.7 Found plate_1: ", found_items[0].text, "plate_1")
        else:
            verificationErrors.append("J1.1.7 Plate_1 are not found")
        search_input(self).clear()
        search_term = "tag='tag 0'"
        for x in range(0, len(search_term)):
            search_input(self).send_keys(search_term[x])
        search_input(self).send_keys(Keys.ENTER)
        sleep(2)
        found_items = search_found_list(self)
        for item in found_items:
            print("    found: " + item.text)
        compare_values(self, "J1.1.8 Found items: ", len(found_items), 1)
        if len(found_items) > 0:
            compare_values(self, "J1.1.9 Found l-bracket_1: ", found_items[0].text, "l-bracket_1")
        else:
            verificationErrors.append("J1.1.9 L-bracket_1 are not found")
        search_input(self).clear()

        # Remove tags from part_1
        point_setter(self, 96, 63)
        # Wait until tags button is available
        wait_condition_located(self, properties_button_clickable, 15)
        properties_button(self).click()
        sleep(1)
        wait_condition_clickable(self, tags_button_clickable, 10)
        tags_button(self).click()
        sleep(1)
        # Select tag
        tags_list(self)[0].click()
        sleep(1)
        # Wait until tags delete button is available
        wait_condition_clickable(self, tag_delete_clickable, 10)
        tag_delete_button(self).click()
        sleep(1)
        # Select tag
        tags_list(self)[0].click()
        sleep(1)
        # Wait until tags delete button is available
        wait_condition_clickable(self, tag_delete_clickable, 10)
        tag_delete_button(self).click()
        sleep(1)
        # Check if number of tags for part_1 is equal to initial
        cur_tags_part_1 = len(tags_list(self))
        compare_values(self, "J1.1.10 Tags from part_1 were removed: ", cur_tags_part_1, n_tags_part_1)

        # Select part_2 of model
        point_setter(self, 115, 35)

        # Remove tags from part_2
        tags_list(self)[0].click()
        sleep(1)
        # Wait until delete tags button is available
        wait_condition_clickable(self, tag_delete_clickable, 10)
        tag_delete_button(self).click()
        sleep(1)

        # Check if number of tags for part_2 is equal to initial
        cur_tags_part_2 = len(tags_list(self))
        compare_values(self, "J1.1.11 Tags from part_2 were removed: ", cur_tags_part_2, n_tags_part_2)
        sleep(1)

        # Close dialog
        # Wait until close tags button is available
        wait_condition_clickable(self, close_tag_clickable, 10)
        close_tag_button(self).click()
        # Click on empty space
        point_setter(self, 30, 30)

        print("\nJ1.2 Removing measurement (distance and diameter) from CAD model by tags")

        # Add measurements to the model
        # create_a_measure(self, 92, 90, 128, 55, simple_distance=True)
        # # Create diameter
        # create_a_measure(self, 95, 48, diameter=True)
        create_a_measure(self, 92, 90, 128, 55, simple_distance=True)
        # Create diameter
        create_a_measure(self, 83, 42, diameter=True)

        # Get number of existing measurements
        # Wait until content measurements appears
        wait_condition_located(self, list_measurements, 10)
        content_measurements_open_button(self).click()
        sleep(1)
        is_exist(self, xpath="//project-tree/div/cdk-virtual-scroll-viewport/div/div/div/div/span/span",
                 object_name="distance 0.02",
                 positive_message="J1.2.1 First measurement (simple distance) was added",
                 negative_message="J1.2.1 First measurement was not added")
        is_exist(self, xpath="//project-tree/div/cdk-virtual-scroll-viewport/div/div/div/div/span/span",
                 object_name="diameter 0.01 m",
                 positive_message="J1.2.1 Second measurement (diameter) was added",
                 negative_message="J1.2.1 Diameter was not added")
        sleep(1)
        # Select distance
        point_setter(self, 114, 71)

        # Select tags button
        # Wait until tags button is available
        wait_condition_clickable(self, tags_button_clickable, 10)
        tags_button(self).click()
        sleep(2)
        # Wait until add tags button is available
        wait_condition_clickable(self, add_tag_clickable, 10)
        add_tag_button(self).click()
        sleep(2)
        # Wait until tag input field is available
        wait_condition_located(self, tag_input_available, 10)
        tag_input(self).send_keys("Distance")
        sleep(2)
        # Wait until add tags ok button is available
        wait_condition_clickable(self, tag_ok_clickable, 10)
        tag_ok_button(self).click()
        sleep(2)

        # Select diameter
        point_setter(self, 83, 42)

        # Wait until add tags button is available
        wait_condition_clickable(self, add_tag_clickable, 10)
        add_tag_button(self).click()
        sleep(2)
        # Wait until tag input field is available
        wait_condition_located(self, tag_input_available, 10)
        tag_input(self).send_keys("Diameter")
        sleep(2)
        # Wait until add tags ok button is available
        wait_condition_clickable(self, tag_ok_clickable, 10)
        tag_ok_button(self).click()
        sleep(2)
        # Close tags dialog
        # Wait until close tags button is available
        wait_condition_clickable(self, close_tag_clickable, 10)
        close_tag_button(self).click()
        sleep(2)
        # Wait until measure button is available
        wait_condition_clickable(self, measure_button_clickable, 10)
        # Get number of existing measurements
        # Wait until content measurements appears
        wait_condition_located(self, content_measurements_available, 10)
        # Unselect diameter
        point_setter(self, 120, 120)

        # Open search-box
        search_term = "type=distance || type=diameter"
        for x in range(0, len(search_term)):
            search_input(self).send_keys(search_term[x])
        search_input(self).send_keys(Keys.ENTER)
        sleep(2)
        found_tags = search_found_list(self)
        for tag in found_tags:
            print("    found: " + tag.text)
        compare_values(self, "J1.2.2 Found items: ", len(found_tags), 2)
        # Remove one measurement
        find_element_xpath(self, "//mat-row/mat-cell/span[contains(., '0.01 m')]").click()
        sleep(1)
        delete_button(self).click()
        sleep(1)
        wait_condition_clickable(self, confirm_remove_clickable, 10)
        confirm_remove_button(self).click()
        sleep(2)
        # Check if number of measurements has decreased by 1

        if not is_element_present(self, By.XPATH, "//project-tree//span[contains(., 'diameter 0.01 m')]"):
            print("J1.2.3 One measurement was removed")
        else:
            raise TimeoutException(msg="J1.2.3 One measurement was not removed")
        search_input(self).clear()
        search_term = "type=distance || type=diameter"
        for x in range(0, len(search_term)):
            search_input(self).send_keys(search_term[x])
        search_input(self).send_keys(Keys.ENTER)
        sleep(2)
        found_tags = search_found_list(self)
        for tag in found_tags:
            print("    found: " + tag.text)
        compare_values(self, "J1.2.4 Found items: ", len(found_tags), 1)

        # Remove one measurement
        find_element_xpath(self,
                           "//mat-row/mat-cell[contains(@class,'mat-column-remove')]/button-icon/div/button").click()
        wait_condition_clickable(self, confirm_remove_clickable, 15)
        confirm_remove_button(self).click()

        # Check if number of measurements has decreased by 2
        if not is_element_present(self, By.XPATH, list_measurements):
            print("J1.2.5 All measurements were removed by searching using tags")
        else:
            raise TimeoutException(msg="J1.2.5 All measurements were not removed")
        sleep(1)

        cleanup(self, "Unnamed")
        # tear_down(self)
