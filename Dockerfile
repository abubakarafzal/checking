# # Use an official Python runtime as a parent image
# FROM python:3.8-slim-buster

# # Set the working directory in the container to /app
# WORKDIR /app

# # Add the current directory contents into the container at /app
# ADD . /app

# # Install Docker and Docker Compose
# RUN apt-get update && \
#     apt-get install -y apt-transport-https ca-certificates curl software-properties-common && \
#     curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
#     add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
#     apt-get update && \
#     apt-get install -y docker-ce && \
#     curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
#     chmod +x /usr/local/bin/docker-compose

# # Run docker-compose up when the container launches
# CMD ["docker-compose", "up"]


FROM python:3.11-bookworm

WORKDIR /app

# Add the current directory contents into the container at /app
ADD . /app

# Install Xvfb and other dependencies
RUN apt-get update && \
    apt-get install -y xvfb && \
    apt-get clean

# Set the DISPLAY environment variable
ENV DISPLAY=:99
RUN chmod +x start-tests.sh
# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Run Xvfb and pytest when the container launches
CMD ["./start-tests.sh"]