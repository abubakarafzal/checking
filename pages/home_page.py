from typing import Tuple

import allure
from selenium.webdriver.common.by import By
from time import sleep

from pages.base_page import BasePage
# from Common import *
from utils.Common import *

from dotenv import load_dotenv
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.top_bars.top_menu_bar import TopMenuBar


class HomePage(BasePage):
    """Login Page"""

    USERNAME_FIELD: Tuple[str, str] = (By.CSS_SELECTOR, "input[data-placeholder*='Username']")
    PASSWORD_FIELD: Tuple[str, str] = (By.CSS_SELECTOR, "input[data-placeholder*='Password']")
    LOGIN_BUTTON: Tuple[str, str] = (By.CSS_SELECTOR, "button[type=submit]")
    LOGIN_ERROR_MESSAGE: Tuple[str, str] = (By.CSS_SELECTOR, "div.alert-danger")
    PAGE_TITLE: Tuple[str, str] = (By.CSS_SELECTOR, ".e-form-heading")
    INDICATOR_LODS: Tuple[str, str] = (By.XPATH, "//indicator-lods/mat-progress-bar")
    FORGOT_PASSWORD_LINK: Tuple[str, str] = (
        By.CSS_SELECTOR,
        "[href='https://app.involve.me/password/reset']",
    )

    def __init__(self, driver, wait):
        super().__init__(driver, wait)

    def new_project_link_model(self, link_name):
        """
        This function adds a link for a model, opens the project, checks if the model has been loaded,
        enables the edit mode and waits for the buttons to become available to click.
        """
        create_project_with_upload_as_link(self, link_name, project_name="Unnamed")
        sleep(2)
        # Wait until model uploaded
        wait_until_model_uploaded(self)
        open_project(self, "Unnamed")
        sleep(1)
        # Wait until measure button will be available
        self.wait_condition_clickable(measure_button_clickable, 160)
        content_button(self).click()
        sleep(1)
        # Wait until Models menu button appears
        self.wait_condition_located(list_cad_models, 120)
        content_cad_models_open(self).click()
        # Check that model will be loaded
        get_model_name = link_name.split("/")[-1]
        if not is_element_present(self, By.XPATH, f"//project-tree//span[contains(., '{get_model_name}')]"):
            raise TimeoutException(msg="Model was not loaded!")
        # Wait until edit button will be available
        self.wait_condition_clickable(edit_mode_button_clickable, 60)
        edit_mode_button(self).click()
        sleep(1)
        # Wait until Move & Rotate button will be available
        self.wait_condition_clickable(move_and_rotate_button_clickable, 120)
        sleep(2)

    def use_existing_or_new_project_link_model(self, link_name, project_name):
        """
        This function adds a link for a model, opens the project, checks if the model has been loaded,
        enables the edit mode and waits for the buttons to become available to click.
        """

        try:
            first_project_element = WebDriverWait(self.driver, 30).until(
                EC.presence_of_element_located((By.XPATH, f"(//p[contains(text(),'{project_name}')])[1]"))
            )
            # Click the element
            first_project_element.click()
        except TimeoutException:
            print(f"Element with xpath (//p[contains(text(),'{project_name}')])[1] was not present after 20 seconds")

            create_project_with_upload_as_link(self, link_name, project_name=project_name)
        # Wait until model uploaded
            wait_until_model_uploaded(self)
            open_project(self, project_name)
        # Wait until measure button will be available
        self.wait_condition_clickable(measure_button_clickable, 160)
        content_button(self).click()
        # Wait until Models menu button appears
        self.wait_condition_located(list_cad_models, 120)
        content_cad_models_open(self).click()
        # Check that model will be loaded
        get_model_name = link_name.split("/")[-1]
        if not is_element_present(self, By.XPATH, f"//project-tree//span[contains(., '{get_model_name}')]"):
            raise TimeoutException(msg="Model was not loaded!")
        # Wait until edit button will be available
        self.wait_condition_clickable(edit_mode_button_clickable, 60)
        edit_mode_button(self).click()
        # Wait until Move & Rotate button will be available
        self.wait_condition_clickable(move_and_rotate_button_clickable, 120)

    def canvas_loaded(self):
        """
        This function checks if the canvas is loaded.
        """
        self.wait_condition_until_located(self.INDICATOR_LODS, 10)
        self.wait_condition_until_not_located(self.INDICATOR_LODS, 60)
