from typing import Tuple, Union

from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver import ActionChains, Chrome, Edge, Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.expected_conditions import (
    StaleElementReferenceException,
)
from selenium.webdriver.support.wait import WebDriverWait

from selenium.webdriver.support import expected_conditions as EC
import json
from pathlib import Path


class BasePage:
    """Wrapper for selenium operations"""

    def __init__(self, driver: Union[Chrome, Firefox, Edge], wait: WebDriverWait):
        self.driver = driver
        self.wait = wait

    def edit_cookie(self, cookie_key: str, cookie_value: str):
        cookie = self.wait.until(
            lambda d: d.get_cookie(cookie_key),
            message=f"Cookie '{cookie_key}' does not exist within the given timeout",
        )
        self.driver.delete_cookie(cookie_key)
        self.driver.add_cookie(
            {
                "name": cookie["name"],
                "value": cookie_value,
                "domain": cookie["domain"],
                "path": cookie["path"],
                "secure": cookie["secure"],
                "expiry": (cookie["expiry"]),
            }
        )

    def click(self, locator: Tuple[str, str]) -> None:
        el: WebElement = self.wait.until(
            expected_conditions.element_to_be_clickable(locator)
        )
        self._highlight_element(el, "green")
        el.click()

    def fill_text(self, locator: Tuple[str, str], txt: str) -> None:
        el: WebElement = self.wait.until(
            expected_conditions.element_to_be_clickable(locator)
        )
        el.clear()
        self._highlight_element(el, "green")
        el.send_keys(txt)

    def clear_text(self, locator: Tuple[str, str]) -> None:
        el: WebElement = self.wait.until(
            expected_conditions.element_to_be_clickable(locator)
        )
        el.clear()

    def scroll_to_bottom(self) -> None:
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    def submit(self, webelement: WebElement) -> None:
        self._highlight_element(webelement, "green")
        webelement.submit()

    def get_text(self, locator: Tuple[str, str]) -> str:
        el: WebElement = self.wait.until(
            expected_conditions.visibility_of_element_located(locator)
        )
        self._highlight_element(el, "green")
        return el.text

    def move_to_element(self, webelement: WebElement) -> None:
        action = ActionChains(self.driver)
        self.wait.until(expected_conditions.visibility_of(webelement))
        action.move_to_element(webelement).perform()

    def is_elem_displayed(self, webelement: WebElement) -> bool:
        try:
            return webelement.is_displayed()
        except StaleElementReferenceException:
            return False
        except NoSuchElementException:
            return False

    def _highlight_element(self, webelement: WebElement, color: str) -> None:
        original_style = webelement.get_attribute("style")
        new_style = f"background-color:yellow;border: 1px solid {color}{original_style}"
        self.driver.execute_script(
            "var tmpArguments = arguments;setTimeout(function () {tmpArguments[0].setAttribute('style', '"
            + new_style
            + "');},0);",
            webelement,
        )
        self.driver.execute_script(
            "var tmpArguments = arguments;setTimeout(function () {tmpArguments[0].setAttribute('style', '"
            + original_style
            + "');},400);",
            webelement,
        )

    def wait_condition_clickable(self, element_name: str, time: int) -> None:
        """
        This function waits until the element becomes clickable.
        """
        WebDriverWait(self.driver, time).until(EC.element_to_be_clickable((By.XPATH, element_name)))

    def wait_condition_located(self, element_name: str, time: int) -> None:
        """
        This function waits until the element appears in its place.
        """
        WebDriverWait(self.driver, time).until(EC.presence_of_element_located((By.XPATH, element_name)))

    def wait_condition_until_not_located(self, locator: Tuple[str, str], time) -> None:
        try:
            wait = WebDriverWait(self.driver, time, poll_frequency=0.5)
            wait.until_not(EC.presence_of_element_located(locator))
        except (TimeoutException, NoSuchElementException):
            print(f"Element with xpath (//p[contains(text(),'{locator}')])[1] still Exists")

    def wait_condition_until_located(self, locator: Tuple[str, str], time) -> None:

        try:
            wait = WebDriverWait(self.driver, time, poll_frequency=0.5)
            wait.until(EC.presence_of_element_located(locator))
        except (TimeoutException, NoSuchElementException):
            print(f"Element with xpath (//p[contains(text(),'{locator}')])[1] does not exist")

    def find_element_xpath(self, xpath: str) -> WebElement:

        return self.driver.find_element(By.XPATH, value=xpath)

    def find_elements_by_xpath(self, xpath: str) -> list:
        return self.driver.find_elements(By.XPATH, value=xpath)
