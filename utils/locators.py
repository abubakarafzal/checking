from selenium.webdriver.common.by import By


# for maintainability we can seperate web objects by page name




class LoginPageLocators(object):
    EMAIL = (By.ID, 'email')
    PASSWORD = (By.ID, 'apassword')
    SUBMIT = (By.ID, 'signInSubmit-input')
    ERROR_MESSAGE = (By.ID, 'message_error')
