import json
import os
import time

from PIL import Image
import pyautogui

import psutil
from dotenv import load_dotenv
from selenium import webdriver
from selenium.common.exceptions import (NoAlertPresentException,
                                        NoSuchElementException,
                                        TimeoutException)
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait

from utils import users

load_dotenv()


def wait_condition_located(self, element_name: str, time: int) -> None:
    """
    This function waits until the element appears in its place.
    """
    WebDriverWait(self.driver, time).until(EC.presence_of_element_located((By.XPATH, element_name)))


def wait_condition_clickable(self, element_name: str, time: int) -> None:
    """
    This function waits until the element becomes clickable.
    """
    WebDriverWait(self.driver, time).until(EC.element_to_be_clickable((By.XPATH, element_name)))


def find_element_xpath(self, xpath: str) -> WebElement:
    return self.driver.find_element(By.XPATH, value=xpath)


def find_element_xpath(self, xpath: str) -> WebElement:
    return self.driver.find_element(By.XPATH, value=xpath)


def find_elements_by_xpath(self, xpath: str) -> list:
    return self.driver.find_elements(By.XPATH, value=xpath)


#                                                    BUTTONS
# Sign menu in button:
########################################################################################################################
def username_input(self) -> WebElement:
    return find_element_xpath(self, "//sign-in//span[contains(., 'Username')]/..//input")


def password_input(self):
    return find_element_xpath(self, "//sign-in//span[contains(., 'Password')]/..//input")


def setUp(self):
    sel_dir = os.getenv("SELENIUM_DIR")
    if self.capabilities["browserName"] == "Chrome":
        chrome_options = Options()
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-notifications")
        chrome_options.add_argument("--ignore-certificate-errors")  # for qa cloud version
        # chrome_options.add_argument("--start-maximized")
        chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
        chrome_options.add_experimental_option('useAutomationExtension', False)
        chrome_options.add_experimental_option(
            'prefs', {
                'credentials_enable_service': False,
                'profile': {
                    'password_manager_enabled': False
                }
            }
        )
        # options.add_argument("user-data-dir=C:/Users/imv/AppData/Local/Google/Chrome/User Data/Default")
        # options.add_argument("--window-size=1920,980")
        # options.add_argument('auto-open-devtools-for-tabs')
        self.driver = webdriver.Chrome(sel_dir, chrome_options=chrome_options)
        # self.driver.set_window_position(0, 0)
        # self.driver.set_window_size(1920, 1080)
    if self.capabilities["browserName"] == "Firefox":
        options = webdriver.FirefoxOptions()
        options.add_argument("--disable-infobars ")
        self.driver = webdriver.Firefox(sel_dir, options=options)
    self.driver.set_window_position(0, 0)
    self.driver.set_window_size(1920, 1055)

    self.driver.implicitly_wait(3)
    self.base_url = "https://www.katalon.com/"
    self.verificationErrors = []
    self.accept_next_alert = True

    driver = self.driver
    driver.get(os.getenv("SIGN_URL"))

    user = driver.find_element_by_id("mat-input-0")
    user.send_keys("MrAA")
    time.sleep(1)
    passwd = driver.find_element_by_id("mat-input-1")
    passwd.send_keys("Test1234!")
    time.sleep(1)
    driver.find_element_xpath("//div[contains(@class, 'loader-button')]/button").click()
    time.sleep(3)
    os.makedirs('output', exist_ok=True)


def set_up_cloud(self, username: str, password: str):
    os.makedirs('Test_report/output', exist_ok=True)


def loginToApp(self, user):
    user = users.get_user(user)
    print(user)
    print(user["email"])

    driver = self.driver
    driver.switch_to.window(driver.current_window_handle)
    if is_element_present(self, By.XPATH, local_clickable):
        local_button(self).click()
        time.sleep(2)
        wait_condition_clickable(self, forgot_password_clickable, 15)
    # Enter the username
    driver.find_element(By.XPATH, emailElem).send_keys("MrAA")
    # driver.find_element(emailElem).send_keys(user["email"])
    # username_input(self)
    # username_input(self).send_keys(user["email"])
    time.sleep(1)
    driver.find_element(By.XPATH, passwordElem).send_keys("Test1234!")
    # Enter the password
    time.sleep(1)
    # Wait until sign in button will be clickable
    wait_condition_clickable(self, sign_in_button_clickable, 15)
    sign_in_button().click()
    time.sleep(2)
    # Wait until profile button appears:
    wait_condition_clickable(self, element_name=user_menu_button_clickable, time=15)
    time.sleep(3)


def useProxy(self):
    sel_dir = os.getenv("SELENIUM_DIR")

    if self.capabilities["browserName"] == "Chrome":
        # server = Server(sel_dir + '/browsermob-proxy-2.1.4/bin/browsermob-proxy.bat', options={'port': 8092})
        # server.start()
        # proxy = server.create_proxy()
        # proxyPort = proxyServer.getPort()
        # print("01: %s" % (proxyPort))

        chrome_options = Options()
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-notifications")
        # chrome_options.add_argument("--proxy-server={0}".format(proxy.proxy))
        chrome_options.add_argument("--proxy-pac-url=http://localhost/proxy.pac")
        chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
        chrome_options.add_experimental_option('useAutomationExtension', False)
        chrome_options.add_experimental_option(
            'prefs', {
                'credentials_enable_service': False,
                'profile': {
                    'password_manager_enabled': False
                }
            }
        )
        self.driver = webdriver.Chrome(sel_dir, chrome_options=chrome_options)

    self.driver.set_window_position(0, 0)
    self.driver.set_window_size(1920, 1055)

    self.driver.implicitly_wait(3)
    self.base_url = "https://www.katalon.com/"
    self.verificationErrors = []
    self.accept_next_alert = True

    driver = self.driver
    # proxy.new_har("ASRV")

    # proxy.new_har('ASRV', options={'captureHeaders': True, 'captureContent': True})
    driver.get(os.getenv("SIGN_URL"))

    user = driver.find_element_by_id("mat-input-0")
    user.send_keys("MrAA")
    time.sleep(1)
    passwd = driver.find_element_by_id("mat-input-1")
    passwd.send_keys("Test1234!")
    time.sleep(1)
    driver.find_element_xpath("//div[contains(@class, 'loader-button')]/button").click()
    time.sleep(4)
    os.makedirs('output', exist_ok=True)

    # return proxy
    # chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument("--proxy-server={}".format(proxy.proxy))
    # driver = webdriver.Chrome(options=chrome_options)


DATA_DIR_Cloud = "https://asrvfiles.opencascade.com/hybrid/PUBLIC/"
# Define variable used in explicit waits procedures


second_shape = "//div/div/project-viewer/viewerui/mat-drawer-container/mat-drawer/div/div/project-content-base/div/mat-nav-list/selection-list-item[2]"
third_shape = "//div/div/project-viewer/viewerui/mat-drawer-container/mat-drawer/div/div/project-content-base/div/mat-nav-list/selection-list-item[3]"
forth_shape = "//div/div/project-viewer/viewerui/mat-drawer-container/mat-drawer/div/div/project-content-base/div/mat-nav-list/selection-list-item[4]"
number_shapes = "//project-content-base/div[2]/mat-nav-list/selection-list-item/mat-list-item/div/div[3]/span"

emailElem = "//sign-in//span[contains(., 'Username')]/..//input"
passwordElem = "//sign-in//span[contains(., 'Password')]/..//input"

sign_in_button_clickable = "//div[contains(@class, 'loader-button')]/button[contains(., 'Sign In')]"


def sign_in_button(self):
    return find_element_xpath(self, "//div[contains(@class, 'loader-button')]/button[contains(., 'Sign In')]")


sign_in_button_with_sso_clickable = "//div[contains(@class, 'loader-button')]/button[contains(., 'Sign In with SSO')]"


def sign_in_button_with_sso(self):
    return find_element_xpath(self,
                              "//div[contains(@class, 'loader-button')]/button[contains(., 'Sign In with SSO')]")


forgot_password_clickable = "//sign-in/mat-tab-group//button/span[contains(., 'Forgot password')]"

local_clickable = "//sign-in/mat-tab-group/mat-tab-header/div/div/div/div/div[contains(., 'Local')]"


def local_button(self):
    return find_element_xpath(self,
                              "//sign-in/mat-tab-group/mat-tab-header/div/div/div/div/div[contains(., 'Local')]")


# Project Management buttons:
########################################################################################################################


# define main application items
def open_project(self, project_name):
    wait_condition_clickable(self, f"//projects-item/mat-list/cdk-virtual-scroll-viewport/div/div/div/div/"
                                   f"div[@class='project-info']/div[p[contains(., '{project_name}')]]/../../..//"
                                   f"button-basic[@innerlabel='PROJECT_OPEN']", 15)

    find_element_xpath(self, f"//projects-item/mat-list/cdk-virtual-scroll-viewport/div/div/div/div/"
                             f"div[@class='project-info']/div[p[contains(., '{project_name}')]]/../../..//"
                             f"button-basic[@innerlabel='PROJECT_OPEN']").click()


def project_in_list(self, project_name):
    return find_elements_by_xpath(self,
                                  "//projects-item/mat-list/div/div/div/div/div[span[contains(., '" + project_name + "')] ]")


expand_menu_clickable = "//div[@class='project-menu']/button-icon[@icon='more-vertical']/div/button"


def expand_menu_project(self, name):
    # xpath = "//div[@class='project-info']/div[span[contains(., '" + name + "')] ]
    # /../../div[@class='project-menu']/button-icon[@icon='more-vertical']"
    time.sleep(10)
    xpath = f"//div[@class='project-info']/div[p[contains(., '{name}')] ]/../../" \
            f"div[@class='project-menu']/button-icon[@icon='more-vertical']"

    return find_element_xpath(self, xpath)


create_project_clickable = "//button[span[contains(.,'Create')]]"


def create_project_button(self):
    return find_element_xpath(self, "//button[span[contains(.,'Create')]]")


add_project_clickable = "//app-toolbar//button/span/mat-icon[@data-mat-icon-name='add']"


def add_project_button(self):
    return find_element_xpath(self, "//app-toolbar//button/span/mat-icon[@data-mat-icon-name='add']")


create_new_project_clickable = "//projects/div/div/div/button-raised/button[contains(., 'New project')]"


def create_new_project_button(self):
    return find_element_xpath(self, "//projects/div/div/div/button-raised/button[contains(., 'New project')]")


duplicate_project_clickable = "//div/button-basic[@innerlabel='PROJECT_DUPLICATE']/button"


def duplicate_project_button(self):
    return find_element_xpath(self, "//div/button-basic[@innerlabel='PROJECT_DUPLICATE']/button")


confirm_duplicate_clickable = "//projects-copy/form/mat-dialog-actions/div/button[span[contains(., 'Duplicate')]]"


def confirm_duplicate_button(self):
    return find_element_xpath(self,
                              "//projects-copy/form/mat-dialog-actions/div/button[span[contains(., 'Duplicate')]]")


cancel_duplicate_clickable = "//projects-copy/form/mat-dialog-actions/div/button[span[contains(., 'Cancel')]]"


def cancel_duplicate_button(self):
    return find_element_xpath(self,
                              "//projects-copy/form/mat-dialog-actions/div/button[contains(.,'Cancel')]")


edit_project_button_clickable = "//div/button[span[contains(., 'Edit')]]"


def edit_project_button(self):
    return find_element_xpath(self, "//div/button[span[contains(., 'Edit')]]")


share_project_button_clickable = "//div//button[contains(., 'Share')]"


def share_project_button(self):
    return find_element_xpath(self, "//div//button[contains(., 'Share')]")


ownership_project_button_clickable = "//div//button[contains(., 'Ownership')]"


def ownership_project_button(self):
    return find_element_xpath(self, "//div//button[contains(., 'Ownership')]")


remove_project_clickable = "//div/button[span[contains(., 'Delete')]]"


def remove_project_button(self):
    return find_element_xpath(self, "//div/button[span[contains(., 'Delete')]]")


manage_poi_types_button_clickable = "//configuration-menu-content/mat-card//button[contains(., 'Manage POI types')]"


def manage_poi_types_button(self):
    return find_element_xpath(self,
                              "//configuration-menu-content/mat-card//button[contains(., 'Manage POI types')]")


cancel_remove_clickable = "//confirm-dialog/mat-dialog-actions/div/button-basic/button[contains(., 'Cancel')]"


def cancel_remove_button(self):
    return find_element_xpath(self,
                              "//confirm-dialog/mat-dialog-actions/div/button-basic/button[contains(., 'Cancel')]")


confirm_remove_clickable = "//confirm-dialog/mat-dialog-actions/div/button-raised/button/span[contains(., 'Delete')]"


def confirm_remove_button(self):
    return find_element_xpath(self,
                              "//confirm-dialog/mat-dialog-actions/div/button-raised/button/span[contains(., 'Delete')]")


project_name_textarea_available = "//projects-embedded//span[contains(., 'Project name')]/..//input"


def project_name_textarea(self):
    return find_element_xpath(self,
                              "//projects-embedded//span[contains(., 'Project name')]/..//input")


project_description_textarea_available = "//projects-embedded//span[contains(., 'Project description')]/..//textarea"


def project_description_textarea(self):
    return find_element_xpath(self,
                              "//projects-embedded//span[contains(., 'Project description')]/..//textarea")


def project_description_in_list(self, description):
    x_str = "//projects-item/mat-list/div[@class='ng-star-inserted']/div/div/div[@class='project-info']" \
            "/p[contains(., \'" + description + "\')]"
    return find_elements_by_xpath(self, x_str)


save_project_button_clickable = "//button[@type='submit']"


def save_project_button(self):
    return find_element_xpath(self, "//button[@type='submit']")


def save_button(self):
    return find_element_xpath(self, "//button[@type='submit']")


def back_to_project_list(self):
    return find_element_xpath(self, "//app-toolbar//button/span/mat-icon[@data-mat-icon-name='back']")


item_view_clickable = "//app-root//button/span/mat-icon[@data-mat-icon-name='item-view']"


def item_view_button(self):
    return find_element_xpath(self, "//app-root//button/span/mat-icon[@data-mat-icon-name='item-view']")


list_view_clickable = "//app-root//button/span/mat-icon[@data-mat-icon-name='list-view']"


def list_view_button(self):
    return find_element_xpath(self, "//app-root//button/span/mat-icon[@data-mat-icon-name='list-view']")


items_mode_clickable = "//div/div/button/span[contains(., 'Item')]"


def items_mode_button(self):
    return find_element_xpath(self, "//div/div/button/span[contains(., 'Item')]")


lists_mode_clickable = "//div/div/button/span[contains(., 'List')]"


def lists_mode_button(self):
    return find_element_xpath(self, "//div/div/button/span[contains(., 'List')]")


def choose_project_list_view(self, items_mode=None, lists_mode=None):
    if items_mode:
        list_view_button(self).click()
        time.sleep(1)
        items_mode_button(self).click()
    if lists_mode:
        item_view_button(self).click()
        time.sleep(1)
        lists_mode_button(self).click()
    time.sleep(1)


def expand_upload_parameters(self):
    return find_element_xpath(self,
                              "//upload-embedded//mat-expansion-panel-header/span[contains(., 'Parameters')]/../"
                              "span[@style='transform: rotate(0deg);']")


def import_as_one_object_switch(self):
    return find_element_xpath(self,
                              "//upload-embedded//mat-slide-toggle/label[contains(., 'Import as one object')]/.."
                              "//div[@class='mat-slide-toggle-bar']")


def increase_rendering_efficiency_switch(self):
    return find_element_xpath(self,
                              "//upload-embedded//mat-slide-toggle/label[contains(., 'Increase rendering efficiency ')]/.."
                              "//div[@class='mat-slide-toggle-bar']")


def choose_actions_on_conflicting_poi_objects(self, update_position_and_poi_information=None,
                                              delete_poi_objects_and_recreate_them=None):
    find_element_xpath(self,
                       "//upload-embedded/form/div/mat-expansion-panel//mat-select/div/div[2]").click()
    time.sleep(1)
    if update_position_and_poi_information:
        find_element_xpath(self, "//mat-option/span[contains(., 'Update position and POI information')]").click()
    if delete_poi_objects_and_recreate_them:
        find_element_xpath(self, "//mat-option/span[contains(., 'Delete POI objects and recreate them')]").click()
    time.sleep(1)


choose_button_clickable = "//upload-embedded//button//mat-icon[@data-mat-icon-name='file-open']"


def choose_button(self):
    return find_element_xpath(self, "//upload-embedded//button//mat-icon[@data-mat-icon-name='file-open']")


link_button_clickable = "//upload-embedded//button//mat-icon[@data-mat-icon-name='link']"


def link_button(self):
    return find_element_xpath(self, "//upload-embedded//button//mat-icon[@data-mat-icon-name='link']")


drop_button_clickable = "//upload-embedded//button//mat-icon[@data-mat-icon-name='cloud-queue']"


def drop_button(self):
    return find_element_xpath(self, "//upload-embedded//button//mat-icon[@data-mat-icon-name='cloud-queue']")


def clip_button(self):
    return find_element_xpath(self, "//mat-tab-header//mat-icon[@data-mat-icon-name='attach']")


def url_address_input(self):
    return find_element_xpath(self, "//upload-url//input[@formcontrolname='urlFile']")


ok_url_button_clickable = "//upload-url//button[contains(., 'Ok')]"


def ok_url_button(self):
    return find_element_xpath(self, "//upload-url//button[contains(., 'Ok')]")


cancel_url_button_clickable = "//upload-url//button[contains(., 'Cancel')]"


def cancel_url_button(self):
    return find_element_xpath(self, "//upload-url//button[contains(., 'Cancel')]")


def upload_button(self):
    return find_element_xpath(self, "//upload/form/mat-dialog-actions/div/button/span[contains(., 'Upload')]")


# Sharing buttons:
########################################################################################################################

def project_reviewer_button(self):
    return find_element_xpath(self, "//mat-option/span[contains(., 'Project reviewer')]")


def project_contributor_button(self):
    return find_element_xpath(self, "//mat-option/span[contains(., 'Project contributor')]")


def project_administrator_button(self):
    return find_element_xpath(self, "//mat-option/span[contains(., 'Project administrator')]")


def sharing_toggle(self):
    return find_element_xpath(self,
                              "//share/form/mat-dialog-content/div/div/section/mat-slide-toggle[@formcontrolname='sharedByLink']")


def optional_field(self):
    return find_element_xpath(self,
                              "//mat-dialog-container/share/form/div/div/div/div/mat-form-field/div/div/div/mat-select/div/div/div")


def sharing_done(self):
    return find_element_xpath(self, "//button/span[contains(., 'Apply')]")


def shared_marker(self):
    return find_elements_by_xpath(self,
                                  "//projects-item//div[@class='project-actions']/mat-icon[@svgicon='share']")


sharing_add_button_clickable = "//mat-dialog-container/share//button/span[contains(., 'Add')]"


def sharing_add_button(self):
    return find_element_xpath(self, "//mat-dialog-container/share//button/span[contains(., 'Add')]")


sharing_toggle_clickable = "//share/form/mat-dialog-content/div/div/section/mat-slide-toggle[@formcontrolname='sharedByLink']"
optional_field_clickable = "//mat-dialog-container/share/form/div/div/div/div/mat-form-field/div/div/div/mat-select/div/div/div"
sharing_done_clickable = "//button/span[contains(., 'Apply')]"

project_reviewer_clickable = "//mat-option/span[contains(., 'Project reviewer')]"
project_contributor_clickable = "//mat-option/span[contains(., 'Project contributor')]"
project_administrator_clickable = "//mat-option/span[contains(., 'Project administrator')]"

# Top panel buttons:
########################################################################################################################

exit_the_project_clickable = '//mat-toolbar/mat-toolbar-row/div/button-icon//mat-icon[@data-mat-icon-name="back"]'


def exit_the_project_button(self):
    return find_element_xpath(self,
                              '//mat-toolbar/mat-toolbar-row/div/button-icon//mat-icon[@data-mat-icon-name="back"]')


imports_upload_button_clickable = "//imports/button-icon/div/span[contains(., 'Upload')]/../button"


def imports_upload_button(self):
    return find_element_xpath(self,
                              "//imports/button-icon/div/span[contains(., 'Upload')]/../button")


assembly_button_clickable = "//assembly-tree/button-icon/div/span[contains(., 'Assembly')]/../button"


def assembly_button(self):
    return find_element_xpath(self,
                              "//assembly-tree/button-icon/div/span[contains(., 'Assembly')]/../button")


content_button_clickable = "//project-content-tree/button-icon/div/span[contains(., 'Content')]/../button"


def content_button(self):
    return find_element_xpath(self,
                              "//project-content-tree/button-icon/div/span[contains(., 'Content')]/../button")


refresh_button_clickable = "//button-icon/div/span[contains(., 'Refresh')]/../button"


def refresh_button(self):
    return find_element_xpath(self, "//button-icon/div/span[contains(., 'Refresh')]/../button")


edit_mode_button_clickable = "//edit-project//button-icon/div/span[contains(., 'Edit Mode')]/../button"


def edit_mode_button(self):
    return find_element_xpath(self, "//edit-project//button-icon/div/span[contains(., 'Edit Mode')]/../button")


configuration_button_clickable = "//configuration-menu/div/button-icon/div/span[contains(., 'Configuration')]/../button"


def configuration_button(self):
    return find_element_xpath(self,
                              "//configuration-menu/div/button-icon/div/span[contains(., 'Configuration')]/../button")


# Search panel buttons:
########################################################################################################################

search_panel_close_clickable = "//app-root//app-toolbar//mat-icon[@data-mat-icon-name='cancel']"


def search_panel_close_button(self):
    return find_element_xpath(self, "//app-root//app-toolbar//mat-icon[@data-mat-icon-name='cancel']")


search_panel_delete_clickable = "//mat-card-content//button/span[contains(., 'Delete')]"


def search_panel_delete_button(self):
    return find_element_xpath(self,
                              "//mat-card-content//button/span[contains(., 'Delete')]")


search_panel_show_clickable = "//mat-card-content//button/span[contains(., 'Show')]"


def search_panel_show_button(self):
    return find_element_xpath(self,
                              "//mat-card-content//button/span[contains(., 'Show')]")


search_panel_show_only_clickable = "//mat-card-content//button/span[contains(., 'Show only')]"


def search_panel_show_only_button(self):
    return find_element_xpath(self,
                              "//mat-card-content//button/span[contains(., 'Show only')]")


search_panel_hide_clickable = "//mat-card-content//button/span[contains(., 'Hide')]"


def search_panel_hide_button(self):
    return find_element_xpath(self,
                              "//mat-card-content//button/span[contains(., 'Hide')]")


search_panel_select_all_clickable = "//mat-card-content//button/span[contains(., 'Select all')]"


def search_panel_select_all_button(self):
    return find_element_xpath(self,
                              "//mat-card-content//button/span[contains(., 'Select all')]")


search_panel_deselect_all_clickable = "//mat-card-content//button/span[contains(., 'Deselect all')]"


def search_panel_deselect_all_button(self):
    return find_element_xpath(self,
                              "//mat-card-content//button/span[contains(., 'Deselect all')]")


def search_input(self):
    return find_element_xpath(self, "//div[@class='search-layout']/form/mat-form-field/div/div/div/input")


def search_found(self):
    return find_element_xpath(self,
                              "//div[@role='listbox']/mat-table/mat-option[@role='option']/span[@class='mat-option-text']/mat-row/mat-cell")


def search_found_list(self):
    return find_elements_by_xpath(self,
                                  "//div[@role='listbox']/mat-table/mat-option[@role='option']/span[@class='mat-option-text']/mat-row/"
                                  "mat-cell[contains(@class,'mat-column-text')]/span[contains(@class, 'item-text')]")


def search_select(self, name):
    find_element_xpath(self, "//span[contains(.,name) and @class='mat-body item-text']").click()


def search_tags(self):
    return find_element_xpath(self, "//mat-option/span[@class='mat-option-text']/span[contains(., 'Tags')]")


def search_annotations(self):
    return find_element_xpath(self,
                              "//mat-option/span[@class='mat-option-text']/span[contains(., 'Annotation')]")


def search_attributes(self):
    return find_element_xpath(self,
                              "//mat-option/span[@class='mat-option-text']/span[contains(., 'Attributes')]")


def search_CAD(self):
    return find_element_xpath(self,
                              "//mat-option/span[@class='mat-option-text']/span[contains(., 'CAD objects')]")


def search_regions(self):
    return find_element_xpath(self, "//mat-option/span[@class='mat-option-text']/span[contains(., 'Region')]")


def search_measurements(self):
    return find_element_xpath(self, "//mat-option/span[@class='mat-option-text']/span[contains(., 'Measure')]")


# Bottom panel buttons:
########################################################################################################################

fit_scene_button_clickable = "//controls//button-fab/div[span[contains(., 'Fit scene')]]/button"


def fit_scene_button(self):
    return find_element_xpath(self,
                              "//controls//button-fab/div[span[contains(., 'Fit scene')]]/button")


clipping_box_button_clickable = "//controls//button-fab/div[span[contains(., 'Clipping Box')]]/button"


def clipping_box_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Clipping Box')]]/button")


view_button_clickable = "//controls//button-fab/div[span[contains(., 'View')]]/button"


def view_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'View')]]/button")


scenario_button_clickable = "//controls//button-fab/div[span[contains(., 'Scenario')]]/button"


def scenario_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Scenario')]]/button")


options_button_clickable = "//controls//button-fab/div[span[contains(., 'Options')]]/button"


def options_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Options')]]/button")


tasks_button_clickable = "//controls//button-fab/div[span[contains(., 'Tasks')]]/button"


def tasks_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Tasks')]]/button")


close_tasks_button_clickable = "//upload-tasks/mat-card//button-icon[@icon='cancel']"


def close_tasks_button(self):
    return find_element_xpath(self, "//upload-tasks/mat-card//button-icon[@icon='cancel']")


undo_button_clickable = "//app-root//controls//undo-redo//mat-icon[@data-mat-icon-name='undo']"


def undo_button(self):
    return find_element_xpath(self, "//app-root//controls//undo-redo//mat-icon[@data-mat-icon-name='undo']"
                                    "/../../../button[@type='button']")


redo_button_clickable = "//app-root//controls//undo-redo//mat-icon[@data-mat-icon-name='redo']"


def redo_button(self):
    return find_element_xpath(self, "//app-root//controls//undo-redo//mat-icon[@data-mat-icon-name='redo']"
                                    "/../../../button[@type='button']")


# Right panel buttons:
########################################################################################################################

properties_button_clickable = "//controls//button-fab/div[span[contains(., 'Properties')]]/button"


def properties_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Properties')]]/button")


attributes_button_clickable = "//controls//button-fab/div[span[contains(., 'Attributes')]]/button"


def attributes_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Attributes')]]/button")


tags_button_clickable = "//controls//button-fab/div[span[contains(., 'Tags')]]/button"


def tags_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Tags')]]/button")


fit_selected_button_clickable = "//controls//button-fab/div[span[contains(., 'Fit selected')]]/button"


def fit_selected_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Fit selected')]]/button")


visibility_button_clickable = "//controls//button-fab/div[span[contains(., 'Visibility')]]/button"


def visibility_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Visibility')]]/button")


visibility_show_clickable = "//controls//button-fab[2]/div[span[contains(., 'Show')]]/button"


def visibility_show_button(self):
    return find_element_xpath(self, "//controls//button-fab[2]/div[span[contains(., 'Show')]]/button")


visibility_hide_clickable = "//controls//button-fab/div[span[contains(., 'Hide')]]/button"


def visibility_hide_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Hide')]]/button")


visibility_show_only_clickable = "//controls//button-fab/div[span[contains(., 'Show only')]]/button"


def visibility_show_only_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Show only')]]/button")


domain_button_clickable = "//controls//button-fab/div[span[contains(., 'Domains')]]/button"


def domain_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Domains')]]/button")


groups_button_clickable = "//controls//button-fab/div[span[contains(., 'Groups')]]/button"


def groups_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Groups')]]/button")


edit_region_name_button_clickable = "//controls//button-fab/div[span[contains(., 'Edit region name')]]/button"


def edit_region_name_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Edit region name')]]/button")


regions_export_button_clickable = "//controls//button-fab/div[span[contains(., 'Export')]]/button"


def regions_export_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Export')]]/button")


delete_button_clickable = "//controls//button-fab/div[span[contains(., 'Delete')]]/button"


def delete_button(self):
    element_path = "//controls//button-fab/div[span[contains(., 'Delete')]]/button"
    element = WebDriverWait(self.driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, element_path))
    )
    return element


export_to_file_button_clickable = "//controls//button-fab/div[span[contains(., 'Export to file')]]/button"


def export_to_file_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Export to file')]]/button")


export_to_project_button_clickable = "//controls//button-fab/div[span[contains(., 'Export to project')]]/button"


def export_to_project_button(self):
    return find_element_xpath(self,
                              "//controls//button-fab/div[span[contains(., 'Export to project')]]/button")


rename_button_clickable = "//controls//button-fab/div[span[contains(., 'Rename')]]/button"


def rename_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Rename')]]/button")


right_panel_share_button_clickable = "//controls//button-fab/div[span[contains(., 'Share')]]/button"


def right_panel_share_button(self):
    return find_element_xpath(self, "//controls//button-fab/div[span[contains(., 'Share')]]/button")


right_panel_back_button_clickable = "/app-root//project-viewer//controls//button-basic/button[contains(., 'Back')]"


def right_panel_back_button(self):
    return find_element_xpath(self,
                              "//app-root//project-viewer//controls//button-basic/button[contains(., 'Back')]")


# Left panel buttons:
########################################################################################################################

annotation_button_clickable = "//app-root//toolset-buttons//annotate//span[contains(., 'Annotation')]/../button"


def annotation_button(self):
    return find_element_xpath(self,
                              "//app-root//toolset-buttons//annotate//span[contains(., 'Annotation')]/../button")


measure_button_clickable = "//app-root//toolset-buttons//measure//span[contains(., 'Measure')]/../button"


def measure_button(self):
    return find_element_xpath(self,
                              "//app-root//toolset-buttons//measure//span[contains(., 'Measure')]/../button")


point_of_interest_button_clickable = "//app-root//toolset-buttons//poi//" \
                                     "span[contains(., 'Point of interest')]/../button"


def point_of_interest_button(self):
    return find_element_xpath(self,
                              "//app-root//toolset-buttons//poi//span[contains(., 'Point of interest')]/../button")


extract_region_button_clickable = "//app-root//toolset-buttons//extract//span[contains(., 'Extract Region')]/../button"


def extract_region_button(self):
    return find_element_xpath(self,
                              "//app-root//toolset-buttons//extract//span[contains(., 'Extract Region')]/../button")


clash_detection_button_clickable = "//app-root//toolset-buttons//clash-detection//" \
                                   "span[contains(., 'Clash Detection')]/../button"


def clash_detection_button(self):
    return find_element_xpath(self,
                              "//app-root//toolset-buttons//clash-detection//span[contains(., 'Clash Detection')]/../button")


move_and_rotate_button_clickable = "//app-root//toolset-buttons//transform" \
                                   "//span[contains(., 'Move & Rotate')]/../button"


def move_and_rotate_button(self):
    return find_element_xpath(self,
                              "//app-root//toolset-buttons//transform//span[contains(., 'Move & Rotate')]/../button")


placement_button_clickable = "//app-root//toolset-buttons//place//span[contains(., 'Placement')]/../button"


def placement_button(self):
    return find_element_xpath(self,
                              "//app-root//toolset-buttons//place//span[contains(., 'Placement')]/../button")


# Tree dots buttons (right upper corner):
########################################################################################################################

three_dots_menu_clickable = "//mat-drawer-content//camera-settings//mat-icon[@data-mat-icon-name='more-horizontal']"


def three_dots_menu_button(self):
    return find_element_xpath(self,
                              "//mat-drawer-content//camera-settings//mat-icon[@data-mat-icon-name='more-horizontal']")


reset_camera_clickable = "//div/button/span[contains(., 'Reset camera')]"


def reset_camera_button(self):
    return find_element_xpath(self, "//div/button/span[contains(., 'Reset camera')]")


model_refresh_clickable = "//div/button/span[contains(., 'Model refresh')]"


def model_refresh_button(self):
    return find_element_xpath(self, "//div/button/span[contains(., 'Model refresh')]")


show_all_clickable = "//div/button/span[contains(., 'Show all')]"


def show_all_button(self):
    return find_element_xpath(self, "//div/button/span[contains(., 'Show all')]")


screenshot_clickable = "//div/button/span[contains(., 'Screenshot')]"


def screenshot_button(self):
    return find_element_xpath(self, "//div/button/span[contains(., 'Screenshot')]")


# User menu buttons:
########################################################################################################################

user_menu_button_clickable = "//app-toolbar//user-menu//mat-icon[@data-mat-icon-name='account']"


def user_menu_button(self):
    return find_element_xpath(self, "//app-toolbar//user-menu//mat-icon[@data-mat-icon-name='account']")


profile_button_clickable = "//user-menu-content//button/mat-icon[@data-mat-icon-name='edit_profile']"


def profile_button(self):
    return find_element_xpath(self, "//user-menu-content//button/mat-icon[@data-mat-icon-name='edit_profile']")


preferences_button_clickable = "//user-menu-content//button/mat-icon[@data-mat-icon-name='settings']"


def preferences_button(self):
    return find_element_xpath(self, "//user-menu-content//button/mat-icon[@data-mat-icon-name='settings']")


change_password_button_clickable = "//user-menu-content//button/mat-icon[@data-mat-icon-name='change_password']"


def change_password_button(self):
    return find_element_xpath(self,
                              "//user-menu-content//button/mat-icon[@data-mat-icon-name='change_password']")


feedback_button_clickable = "//user-menu-content//button/mat-icon[@data-mat-icon-name='feedback']"


def feedback_button(self):
    return find_element_xpath(self, "//user-menu-content//button/mat-icon[@data-mat-icon-name='feedback']")


system_info_button_clickable = "//user-menu-content//button/mat-icon[@data-mat-icon-name='info']"


def system_info_button(self):
    return find_element_xpath(self, "//user-menu-content//button/mat-icon[@data-mat-icon-name='info']")


user_manual_button_clickable = "//user-menu-content//button/mat-icon[@data-mat-icon-name='question']"


def user_manual_button(self):
    return find_element_xpath(self, "//user-menu-content//button/mat-icon[@data-mat-icon-name='question']")


logout_button_clickable = "//user-menu-content//button/mat-icon[@data-mat-icon-name='signout']"


def logout_button(self):
    return find_element_xpath(self, "//user-menu-content//button/mat-icon[@data-mat-icon-name='signout']")


# Options buttons:
########################################################################################################################

fullscreen_mode_switch_clickable = "//settings-dialog//span[contains(., 'Fullscreen mode')]/../mat-slide-toggle"


def fullscreen_mode_switch(self):
    return find_element_xpath(self,
                              "//settings-dialog//span[contains(., 'Fullscreen mode')]/../mat-slide-toggle")


dark_background_switch_clickable = "//settings-dialog//span[contains(., 'Dark background')]/../mat-slide-toggle"


def dark_background_switch(self, in_options=None, in_preferences=None):
    if in_options:
        return find_element_xpath(self,
                                  "//settings-dialog//span[contains(., 'Dark background')]/../mat-slide-toggle")
    if in_preferences:
        return find_element_xpath(self,
                                  "//user-preferences//span[contains(., 'Dark background')]/..//div[@class='mat-slide-toggle-bar']")


minimap_switch_clickable = "//settings-dialog//span[contains(., 'Show minimap')]/../mat-slide-toggle"


def minimap_switch(self, in_options=None, in_preferences=None):
    if in_options:
        return find_element_xpath(self,
                                  "//settings-dialog//span[contains(., 'Show minimap')]/../mat-slide-toggle")
    if in_preferences:
        return find_element_xpath(self,
                                  "//user-preferences//span[contains(., 'Show minimap')]/..//div[@class='mat-slide-toggle-bar']")


z_layer_filter_switch_clickable = "//settings-dialog//span[contains(., 'Z layer filter for minimap')]/../" \
                                  "mat-slide-toggle"


def z_layer_filter_switch(self, in_options=None, in_preferences=None):
    if in_options:
        return find_element_xpath(self,
                                  "//settings-dialog//span[contains(., 'Z layer filter for minimap')]/../mat-slide-toggle")
    if in_preferences:
        return find_element_xpath(self, "//user-preferences//span[contains(., 'Z layer filter for minimap')]/"
                                        "..//div[@class='mat-slide-toggle-bar']")


number_filter_switch_clickable = "//settings-dialog//span[contains(., 'Number')]/../mat-slide-toggle"


def number_filter_switch(self, in_options=None, in_preferences=None):
    if in_options:
        return find_element_xpath(self,
                                  "//settings-dialog//span[contains(., 'Number')]/../mat-slide-toggle")
    if in_preferences:
        return find_element_xpath(self,
                                  "//user-preferences//span[contains(., 'Number')]/..//span[@class='mat-slide-toggle-bar']")


number_filter_input_clickable = "//settings-dialog//div[12]/mat-form-field//input[@type='text']"


def number_filter_input(self, in_options=None, in_preferences=None):
    if in_options:
        return find_element_xpath(self, "//settings-dialog//div[12]/mat-form-field//input[@type='text']")
    if in_preferences:
        return find_element_xpath(self,
                                  "//settings-dialog//user-preferences//div[5]/mat-form-field//input[@type='text']")


distance_filter_input_clickable = "//settings-dialog//div[14]/mat-form-field//input[@type='text']"


def distance_filter_input(self, in_options=None, in_preferences=None):
    if in_options:
        return find_element_xpath(self, "//settings-dialog//div[14]/mat-form-field//input[@type='text']")
    if in_preferences:
        return find_element_xpath(self,
                                  "//settings-dialog//user-preferences//div[7]/mat-form-field//input[@type='text']")


distance_filter_switch_clickable = "//settings-dialog//span[contains(., 'Distance')]/../mat-slide-toggle"


def distance_filter_switch(self, in_options=None, in_preferences=None):
    if in_options:
        return find_element_xpath(self,
                                  "//settings-dialog//span[contains(., 'Distance')]/../mat-slide-toggle")
    if in_preferences:
        return find_element_xpath(self,
                                  "//user-preferences//span[contains(., 'Distance')]/..//div[@class='mat-slide-toggle-bar']")


def choose_3d_display_mode(self, displayed_mode: str):
    find_element_xpath(self,
                       "//settings-dialog//mat-grid-tile[2]//mat-select/div/div[2]").click()
    time.sleep(1)
    if displayed_mode == "shaded_with_wireframe":
        find_element_xpath(self, "//mat-option/span[normalize-space(.)=('Shaded with wireframe')]").click()
    if displayed_mode == "shaded":
        find_element_xpath(self, "//mat-option/span[normalize-space(.)=('Shaded')]").click()
    if displayed_mode == "wireframe":
        find_element_xpath(self, "//mat-option/span[normalize-space(.)=('Wireframe')]").click()
    if displayed_mode == "transparent_with_wireframe":
        find_element_xpath(self, "//mat-option/span[normalize-space(.)=('Transparent with wireframe')]").click()
    if displayed_mode == "transparent":
        find_element_xpath(self, "//mat-option/span[normalize-space(.)=('Transparent')]").click()
    if displayed_mode == "hidden_line_removal":
        find_element_xpath(self, "//mat-option/span[normalize-space(.)=('Hidden line removal')]").click()
    time.sleep(1)


def choose_camera_management(self, displayed_mode: str):
    find_element_xpath(self,
                       "//settings-dialog//mat-grid-tile[4]//mat-select/div/div[2]").click()
    time.sleep(1)
    if displayed_mode == "orthographic":
        find_element_xpath(self, "//mat-option/span[normalize-space(.)=('Orthographic')]").click()
    if displayed_mode == "perspective":
        find_element_xpath(self, "//mat-option/span[normalize-space(.)=('Perspective')]").click()
    if displayed_mode == "first person":
        find_element_xpath(self, "//mat-option/span[normalize-space(.)=('First person')]").click()
    if displayed_mode == "virtual reality":
        find_element_xpath(self, "//mat-option/span[normalize-space(.)=('Virtual reality')]").click()
    time.sleep(1)


def choose_rendering_quality(self, in_options=None, in_preferences=None, low=None, average=None, moderate=None,
                             high=None):
    if in_options:
        find_element_xpath(self,
                           "//settings-dialog//mat-grid-tile[6]//mat-select/div/div[2]").click()
    if in_preferences:
        find_element_xpath(self,
                           "//user-preferences//div[3]//mat-select/div/div[2]").click()
    time.sleep(1)
    if low:
        find_element_xpath(self, "//mat-option/span[contains(., 'Low')]").click()
    if average:
        find_element_xpath(self, "//mat-option/span[contains(., 'Average')]").click()
    if moderate:
        find_element_xpath(self, "//mat-option/span[contains(., 'Moderate')]").click()
    if high:
        find_element_xpath(self, "//mat-option/span[contains(., 'High')]").click()
    time.sleep(1)


def choose_displayed_assembly_name(self, displayed_name: str, in_options=None, in_preferences=None):
    if in_options:
        find_element_xpath(self,
                           "//settings-dialog/mat-dialog-content//div[2]/div[4]//mat-select/div/div[2]").click()
    if in_preferences:
        find_element_xpath(self,
                           "//user-preferences//mat-grid-tile[8]//mat-select/div/div[2]").click()
    time.sleep(1)
    if displayed_name == "Instance name":
        find_element_xpath(self, "//mat-option/span[contains(., 'Instance name')]").click()
    if displayed_name == "Product name":
        find_element_xpath(self, "//mat-option/span[contains(., 'Product name')]").click()
    if displayed_name == "Both":
        find_element_xpath(self, "//mat-option/span[contains(., 'Both')]").click()
    time.sleep(1)


def z_layer_for_minimap_setter(self, automatic=None, manual=None, position_of_level_0=None, height_of_a_floor=None):
    # Switch ON z layer filter
    wait_condition_clickable(self, z_layer_filter_switch_clickable, 15)
    z_layer_filter_switch(self, in_options=True).click()
    time.sleep(1)
    if automatic:
        find_element_xpath(self, "//settings-dialog//mat-radio-button/label/span[contains(., 'Automatic')]").click()
        time.sleep(1)
    if manual:
        find_element_xpath(self, "//settings-dialog//mat-radio-button/label/span[contains(., 'Manual')]").click()
        time.sleep(1)
        if position_of_level_0:
            find_element_xpath(self,
                               "//settings-dialog/mat-dialog-content//mat-grid-tile[2]//input").send_keys(
                f"{position_of_level_0}")
        if height_of_a_floor:
            find_element_xpath(self,
                               "//settings-dialog/mat-dialog-content//mat-grid-tile[4]//input").send_keys(
                f"{height_of_a_floor}")


def poi_display_filters_setter(self, in_options=None, in_preferences=None, number=None, distance=None):
    if in_options:
        if number:
            wait_condition_clickable(self, number_filter_switch_clickable, 15)
            number_filter_switch(self, in_options=True).click()
            time.sleep(1)
            find_element_xpath(self, "//settings-dialog/mat-dialog-content//div[11]//input").send_keys(f"{number}")
        if distance:
            wait_condition_clickable(self, distance_filter_switch_clickable, 15)
            distance_filter_switch(self, in_options=True).click()
            time.sleep(1)
            find_element_xpath(self, "//settings-dialog/mat-dialog-content//div[13]//input").send_keys(f"{distance}")
    if in_preferences:
        if number:
            number_filter_switch(self, in_preferences=True).click()
            time.sleep(1)
            find_element_xpath(self, "//user-preferences//div[5]//input").send_keys(f"{number}")
        if distance:
            distance_filter_switch(self, in_preferences=True).click()
            time.sleep(1)
            find_element_xpath(self, "//user-preferences//div[7]//input").send_keys(f"{distance}")


exit_options_clickable = "//settings-dialog//button//mat-icon[@data-mat-icon-name='cancel']"


def exit_options_button(self):
    return find_element_xpath(self, "//settings-dialog//button//mat-icon[@data-mat-icon-name='cancel']")


options_discard_clickable = "//settings-dialog//button[contains(., 'Discard')]"


def options_discard_button(self):
    return find_element_xpath(self, "//settings-dialog//button[contains(., 'Discard')]")


options_apply_clickable = "//settings-dialog//button[contains(., 'Apply')]"


def options_apply_button(self):
    return find_element_xpath(self, "//settings-dialog//button[contains(., 'Apply')]")


displayed_assembly_name_info_icon = "//settings-dialog/mat-dialog-content//span[contains(., 'Displayed assembly name')]/../mat-icon[@svgicon='info']"


# User preferences:
########################################################################################################################
# General:
def choose_language(self, english=None):
    find_element_xpath(self,
                       "//user-preferences//mat-grid-tile[2]//mat-select/div/div[2]").click()
    time.sleep(1)
    if english:
        find_element_xpath(self, "//mat-option/span[contains(., 'English')]").click()
    time.sleep(1)


def choose_length_units(self, units: str):
    find_element_xpath(self,
                       "//user-preferences//mat-grid-tile[4]//mat-select/div/div[2]").click()
    time.sleep(1)
    if units == "Meters":
        find_element_xpath(self, "//mat-option/span[contains(., 'Meters')]").click()
    if units == "Millimeters":
        find_element_xpath(self, "//mat-option/span[contains(., 'Millimeters')]").click()
    if units == "Feet":
        find_element_xpath(self, "//mat-option/span[contains(., 'Feet')]").click()
    if units == "Inches":
        find_element_xpath(self, "//mat-option/span[contains(., 'Inches')]").click()
    time.sleep(1)


def choose_angular_units(self, units: str):
    find_element_xpath(self,
                       "//user-preferences//mat-grid-tile[6]//mat-select/div/div[2]").click()
    time.sleep(1)
    if units == "Radians":
        find_element_xpath(self, "//mat-option/span[contains(., 'Radians')]").click()
    if units == "Degrees":
        find_element_xpath(self, "//mat-option/span[contains(., 'Degrees')]").click()
    if units == "Turns":
        find_element_xpath(self, "//mat-option/span[contains(., 'Turns')]").click()
    time.sleep(1)


# Display:

show_command_buttons_labels_switch_clickable = "//user-preferences//mat-slide-toggle/label" \
                                               "[contains(., 'Show command buttons labels')]/..//" \
                                               "div[@class='mat-slide-toggle-bar']"


def show_command_buttons_labels_switch(self):
    return find_element_xpath(self,
                              "//user-preferences//mat-slide-toggle/label[contains(., 'Show command buttons labels')]/..//"
                              "div[@class='mat-slide-toggle-bar']")


show_help_messages_switch_clickable = "//user-preferences//mat-slide-toggle/label[contains(., 'Show help messages')]" \
                                      "/..//div[@class='mat-slide-toggle-bar']"


def show_help_messages_switch(self):
    return find_element_xpath(self,
                              "//user-preferences//mat-slide-toggle/label[contains(., 'Show help messages')]/..//"
                              "div[@class='mat-slide-toggle-bar']")


def choose_points_shape(self, circle=None, sphere=None, square=None):
    if circle:
        find_element_xpath(self, "//user-preferences//mat-radio-button/label[contains(., 'Circle')]").click()
    if sphere:
        find_element_xpath(self, "//user-preferences//mat-radio-button/label[contains(., 'Sphere')]").click()
    if square:
        find_element_xpath(self, "//user-preferences//mat-radio-button/label[contains(., 'Square')]").click()
    time.sleep(1)


def choose_reduction_of_the_lod(self, no=None, low=None, average=None, high=None, max=None):
    find_element_xpath(self,
                       "//user-preferences//div[7]//mat-select/div/div[2]").click()
    time.sleep(1)
    if no:
        find_element_xpath(self, "//mat-option/span[contains(., 'No')]").click()
    if low:
        find_element_xpath(self, "//mat-option/span[contains(., 'Low')]").click()
    if average:
        find_element_xpath(self, "//mat-option/span[contains(., 'Average')]").click()
    if high:
        find_element_xpath(self, "//mat-option/span[contains(., 'High')]").click()
    if max:
        find_element_xpath(self, "//mat-option/span[contains(., 'Max')]").click()
    time.sleep(1)


def choose_point_cloud_visualization_settings(self, part=None, building=None, city=None):
    # Click on arrow button for drop-down list with templates
    find_element_xpath(self,
                       "//user-preferences//point-cloud-settings//div[2]//mat-select/div/div[2]").click()
    time.sleep(1)
    if part:
        find_element_xpath(self, "//mat-option/span[contains(., 'Part')]").click()
    if building:
        find_element_xpath(self, "//mat-option/span[contains(., 'Building')]").click()
    if city:
        find_element_xpath(self, "//mat-option/span[contains(., 'City')]").click()
    time.sleep(1)


preferences_save_clickable = "//settings-dialog//button[contains(., 'Save')]"


def preferences_save_button(self):
    return find_element_xpath(self, "//settings-dialog//button[contains(., 'Save')]")


def preferences_discard_button(self):
    return find_element_xpath(self, "//settings-dialog//button[contains(., 'Discard')]")


def point_of_interest_tab(self):
    return find_element_xpath(self,
                              "//user-preferences/mat-tab-group/mat-tab-header/div/div/div/div[contains(., 'Point Of Interest')]")


def display_tab(self):
    return find_element_xpath(self,
                              "//user-preferences/mat-tab-group/mat-tab-header/div/div/div/div[contains(., 'Display')]")


def general_tab(self):
    return find_element_xpath(self,
                              "//user-preferences/mat-tab-group/mat-tab-header/div/div/div/div[contains(., 'General')]")


# Content buttons:
########################################################################################################################

list_annotations = "//project-tree//span[contains(., 'Annotations')]/../..//mat-icon[@data-mat-icon-name='chevron_right']"
list_measurements = "//project-tree//span[contains(., 'Measurements')]/../..//mat-icon[@data-mat-icon-name='chevron_right']"
list_lazer_scans = "//project-tree//span[contains(., 'Scans')]/../..//mat-icon[@data-mat-icon-name='chevron_right']"
list_overall_laser_scan = "//project-tree//span[contains(., 'Overall laser scan')]/../..//mat-icon[@data-mat-icon-name='chevron_right']"
list_regions = "//project-tree//span[contains(., 'Regions')]/../..//mat-icon[@data-mat-icon-name='chevron_right']"
list_cad_models = "//project-tree//span[contains(., 'Models')]/../..//mat-icon[@data-mat-icon-name='chevron_right']"
list_poi = "//project-tree//span[contains(., 'Point of interest')]/../..//mat-icon[@data-mat-icon-name='chevron_right']"
list_groups = "//project-tree//span[contains(., 'Groups')]/../..//mat-icon[@data-mat-icon-name='chevron_right']"
list_views = "//project-tree//span[contains(., 'Views')]/../..//mat-icon[@data-mat-icon-name='chevron_right']"
list_scenarios = "//project-tree//span[contains(., 'Scenarios')]/../..//mat-icon[@data-mat-icon-name='chevron_right']"

content_annotations_available = "//project-tree//span[contains(., 'Annotations')]"
content_measurements_available = "//project-tree//span[contains(., 'Measurements')]"
content_lazer_scans_available = "//project-tree//span[contains(., 'Scans')]"
content_overall_laser_scan_available = "//project-tree//span[contains(., 'Overall laser scan')]"
content_regions_available = "//project-tree//span[contains(., 'Regions')]"
content_cad_models_available = "//project-tree//span[contains(., 'Models')]"
content_poi_available = "//project-tree//span[contains(., 'Points of interest')]"
content_groups_available = "//project-tree//span[contains(., 'Groups')]"
content_views_available = "//project-tree//span[contains(., 'Views')]"
content_scenarios_available = "//project-tree//span[contains(., 'Scenarios')]"


def content_annotations_open_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Annotations')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_annotations_close_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Annotations')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


def content_measurements_open_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Measurements')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_measurements_close_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Measurements')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


def content_regions_open_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Regions')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_regions_close_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Regions')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


def content_poi_open_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Points of interest')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_poi_close_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Points of interest')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


def content_scanning_position_open_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Scanning position')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_scanning_position_close_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Scanning position')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


def content_cad_models_open(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Models')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_cad_models_close(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Models')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


def content_lazer_scans_open(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Scans')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_lazer_scans_close(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Scans')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


def content_overall_laser_scan_open(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Overall laser scan')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_overall_laser_scan_close(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Overall laser scan')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


def content_groups_open_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Groups')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_groups_close_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Groups')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


def content_views_open_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Views')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_views_close_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Views')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


def content_scenarios_open_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Scenarios')]/../..//mat-icon[@data-mat-icon-name='chevron_right']")


def content_scenarios_close_button(self):
    return find_element_xpath(self,
                              "//project-tree//span[contains(., 'Scenarios')]/../..//mat-icon[@data-mat-icon-name='expand_more']")


# Annotation buttons:
########################################################################################################################
annotation_popup_available = "//annotate-input//mat-icon[@data-mat-icon-name='more-vertical']"


def annotation_popup_button(self):
    return find_element_xpath(self, "//annotate-input//mat-icon[@data-mat-icon-name='more-vertical']")


annotation_copy_available = "//button[span[contains(.,'Copy to the clipboard')]]"


def annotation_copy_button(self):
    return find_element_xpath(self, "//button[span[contains(.,'Copy to the clipboard')]]")


def annotation_attach_remove(self):
    return find_element_xpath(self, "//annotate-input//mat-icon[@data-mat-icon-name='cancel']")


annotation_remove_confirm_available = "//annotate-input//mat-card-actions//mat-icon[@data-mat-icon-name='apply']"


def annotation_remove_confirm(self):
    return find_element_xpath(self,
                              "//annotate-input/mat-card/mat-card-actions//mat-icon[@data-mat-icon-name='apply']")


annotation_remove_cancel_available = "//annotate-input//mat-card-actions//mat-icon[@data-mat-icon-name='cancel']"


def annotation_remove_cancel(self):
    return find_element_xpath(self,
                              "//annotate-input/mat-card/mat-card-actions//mat-icon[@data-mat-icon-name='cancel']")


annotation_pin_clickable = "//annotate-input//mat-icon[@data-mat-icon-name='pin']"


def annotation_pin_button(self):
    return find_element_xpath(self, "//annotate-input//mat-icon[@data-mat-icon-name='pin']")


annotation_unpin_clickable = "//annotate-input//mat-icon[@data-mat-icon-name='unpin']"


def annotation_unpin_button(self):
    return find_element_xpath(self, "//annotate-input//mat-icon[@data-mat-icon-name='unpin']")


annotation_attach_available = "//input-file/button[span[contains(., 'Attach')]]"


def annotation_attach_button(self):
    return find_element_xpath(self, "//input-file/button[span[contains(., 'Attach')]]")


def annotation_text(self):
    return find_element_xpath(self,
                              "//mat-card/div/mat-tab-group//textarea[contains (@class, 'mat-input-element')]").get_attribute(
        'value')


def ann_textarea(self):
    return find_element_xpath(self, "//mat-card/div/mat-tab-group//textarea[@formcontrolname='text']")


annotation_color_clickable = "//button[span[contains(., 'Color')]]"


def annotation_color(self):
    return find_element_xpath(self, "//button[span[contains(., 'Color')]]")


annotation_remove_available = "//button[span[contains(., 'Delete')]]"


def annotation_remove(self):
    return find_element_xpath(self, "//button[span[contains(., 'Delete')]]")


annotation_color_blue_value = "//mat-grid-tile[1]/div/div"
annotation_color_green_value = "//mat-grid-tile[2]/div/div"
annotation_color_yellow_value = "//mat-grid-tile[3]/div/div"
annotation_color_red_value = "//mat-grid-tile[4]/div/div"


def annotation_color_value(self, i):
    # return find_element_xpath(self,"//body/div/div/div/div/div/mat-grid-list/div/mat-grid-tile
    # [" + str(i) + "]/figure/div[contains(@class,'mat-menu-item')]")
    return find_element_xpath(self,
                              "//div/mat-grid-list/div/mat-grid-tile[" + str(i) + "]/div/div")


def annotation_attach_download(self):
    return find_element_xpath(self,
                              "//mat-list-item[@class='attachment-item mat-list-item ng-star-inserted']/div[@class='mat-list-item-content']"
                              "/button-icon[@icon='download']/div/button")


# Measure buttons:
########################################################################################################################
orthogonal_direction_clickable = "//measure-distance/button//mat-icon[@data-mat-icon-name='orthogonal']"


def orthogonal_direction_button(self):
    return find_element_xpath(self, "//measure-distance/button//mat-icon[@data-mat-icon-name='orthogonal']")


parallel_direction_clickable = "//measure-distance//button//span[contains(., '||')]"


def parallel_direction_button(self):
    return find_element_xpath(self, "//measure-distance//button//span[contains(., '||')]")


x_direction_clickable = "//measure-distance//button//span[contains(., 'X')]"


def x_direction_button(self):
    return find_element_xpath(self, "//measure-distance//button//span[contains(., 'X')]")


y_direction_clickable = "//measure-distance//button//span[contains(., 'Y')]"


def y_direction_button(self):
    return find_element_xpath(self, "//measure-distance//button//span[contains(., 'Y')]")


z_direction_clickable = "//measure-distance//button//span[contains(., 'Z')]"


def z_direction_button(self):
    return find_element_xpath(self, "//measure-distance//button//span[contains(., 'Z')]")


snap_to_objects_switch_clickable = "//measure-distance/mat-slide-toggle/label/span/input[@role='switch']"


def snap_to_objects_switch_button(self):
    return find_element_xpath(self, "//measure-distance/mat-slide-toggle/label/span/input[@role='switch']")


measure_approve_clickable = "//button-fab[contains (@class, 'mat-lock-measure')]/div/button"


def measure_approve_button(self):
    return find_element_xpath(self, "//button-fab[contains (@class, 'mat-lock-measure')]/div/button")


measure_chain_clickable = "//button-fab[contains (@class, 'mat-chain-measure')]/div/button"


def measure_chain_button(self):
    return find_element_xpath(self, "//button-fab[contains (@class, 'mat-chain-measure')]/div/button")


def choose_measure_type(self, distance=None, diameter=None, angle=None):
    find_element_xpath(self, "//measure-inter//mat-card-content//mat-select[@role='combobox']").click()
    time.sleep(1)
    if distance:
        find_element_xpath(self, "//mat-option/span[contains(., 'Distance')]").click()
    elif diameter:
        find_element_xpath(self, "//mat-option/span[contains(., 'Diameter')]").click()
    elif angle:
        find_element_xpath(self, "//mat-option/span[contains(., 'Angle')]").click()
    time.sleep(1)


back_button_clickable = "//button-fab[@innerlabel='BACK']/div/button"


def back_button(self):
    return find_element_xpath(self, "//button-fab[@innerlabel='BACK']/div/button")


# Point of interest buttons
########################################################################################################################

add_poi_type_clickable = "//configuration-entities//button-basic/button//mat-icon[@data-mat-icon-name='add']"


def add_poi_type_button(self):
    return find_element_xpath(self,
                              "//configuration-entities//button-basic/button//mat-icon[@data-mat-icon-name='add']")


save_custom_poi_clickable = "//create-edit-poi-type//button[contains(., 'Save')]"


def save_custom_poi_button(self):
    return find_element_xpath(self, "//create-edit-poi-type//button[contains(., 'Save')]")


close_manage_poi_type_clickable = "//configuration-entities//button[contains(., 'Close')]"


def close_manage_poi_type_button(self):
    return find_element_xpath(self, "//configuration-entities//button[contains(., 'Close')]")


poi_edit_clickable = "//poi-input/div/resize-drag/mat-card//button/span[contains(., 'Edit')]"


def poi_edit_button(self):
    return find_element_xpath(self,
                              "//poi-input/div/resize-drag/mat-card//button/span[contains(., 'Edit')]")


fpv_play_clickable = "//mat-card-content//span[contains(., 'First person view')]/../..//button-icon[@icon='play']"


def first_person_view_play_button(self):
    return find_element_xpath(self,
                              "//mat-card-content//span[contains(., 'First person view')]/../..//button-icon[@icon='play']")


panoramic_play_clickable = "//mat-card-content//span[contains(., 'Panoramic view')]/../..//button-icon[@icon='play']"


def panoramic_view_play_button(self):
    return find_element_xpath(self,
                              "//mat-card-content//span[contains(., 'Panoramic view')]/../..//button-icon[@icon='play']")


exit_from_view_clickable = "//app-toolbar/mat-toolbar//span/mat-icon[@data-mat-icon-name='cancel']"


def exit_from_view_button(self):
    return find_element_xpath(self,
                              "//app-toolbar/mat-toolbar//span/mat-icon[@data-mat-icon-name='cancel']")


close_poi_menu_clickable = "//mat-card-title/button-icon/div/button/span/mat-icon[@data-mat-icon-name='cancel']"


def close_poi_menu_button(self):
    return find_element_xpath(self,
                              "//mat-card-title/button-icon/div/button/span/mat-icon[@data-mat-icon-name='cancel']")


close_edit_poi_type_clickable = "//create-edit-poi-type//button//mat-icon[@data-mat-icon-name='cancel']"


def close_edit_poi_type_button(self):
    return find_element_xpath(self, "//create-edit-poi-type//button//mat-icon[@data-mat-icon-name='cancel']")


add_attribute_clickable = "//create-edit-poi-type//span[contains(., 'Attribute')]/.." \
                          "//mat-icon[@data-mat-icon-name='add']"


def add_attribute_button(self):
    return find_element_xpath(self,
                              "//create-edit-poi-type//span[contains(., 'Attribute')]/..//mat-icon[@data-mat-icon-name='add']")


upload_icon_clickable = "//create-edit-poi-type//button/span[contains(., 'Upload icon')]"


def upload_icon_button(self):
    return find_element_xpath(self,
                              "//create-edit-poi-type/form/mat-dialog-content/main/input-file/button/span[contains(., 'Upload icon')]")


choose_icon_clickable = "//create-edit-poi-type/form/mat-dialog-content/main//div[@class='icon__option']"


def choose_icon_button(self):
    return find_element_xpath(self,
                              "//create-edit-poi-type/form/mat-dialog-content/main//div[@class='icon__option']")


position_expand_clickable = "//mat-card-content/form/div/mat-label[contains(., 'Position')]/..//button"


def poi_type_name_input(self):
    return find_element_xpath(self, "//mat-dialog-container/create-edit-poi-type//input[@name='type-name']")


def position_expand_button(self):
    return find_element_xpath(self,
                              "//mat-card-content/form/div/mat-label[contains(., 'Position')]/..//button")


# Creation POI dialog:
x_coordinates_input = "//resize-drag//mat-card-content//label/mat-label[contains(., 'x')]/../../../input"
y_coordinates_input = "//resize-drag//mat-card-content//label/mat-label[contains(., 'y')]/../../../input"
z_coordinates_input = "//resize-drag//mat-card-content//label/mat-label[contains(., 'z')]/../../../input"

# Create POI dialog:
text_attribute_input_clickable = "//multimedia-dialog/mat-dialog-content/mat-form-field/div/div/div/textarea"


def text_attribute_input(self):
    return find_element_xpath(self,
                              "//multimedia-dialog/mat-dialog-content/mat-form-field/div/div/div/textarea")


add_image_clickable = "//multimedia-dialog/mat-dialog-content//span[contains(., 'Add images')]"


def add_image_button(self):
    return find_element_xpath(self,
                              "//multimedia-dialog/mat-dialog-content//span[contains(., 'Add images')]")


# Creation tag dialog:
tag_expand_clickable = "//mat-card-content/form/div/mat-label[contains(., 'POI tags')]/..//button"


def tag_expand_button(self):
    return find_element_xpath(self,
                              "//mat-card-content/form/div/mat-label[contains(., 'POI tags')]/..//button")


add_tag_to_poi_clickable = "//poi-input//div[@class='poi-tags']/..//button//mat-icon[@data-mat-icon-name='add']"


def add_tag_to_poi_button(self):
    return find_element_xpath(self,
                              "//poi-input//div[@class='poi-tags']/..//button//mat-icon[@data-mat-icon-name='add']")


add_tag_input = "//poi-input//div[@class='poi-tags']//input"

# Edit tag in POI
edit_poi_tag_clickable = "//poi-input//button/span[contains(., 'Edit')]"


def edit_poi_tag_button(self):
    return find_element_xpath(self, "//poi-input//button/span[contains(., 'Edit')]")


delete_poi_tag_clickable = "//poi-input//button/span[contains(., 'Delete')]"


def delete_poi_tag_button(self):
    return find_element_xpath(self, "//poi-input//button/span[contains(., 'Delete')]")


search_poi_tag_clickable = "//poi-input//button/span[contains(., 'Search')]"


def search_poi_tag_button(self):
    return find_element_xpath(self, "//poi-input//button/span[contains(., 'Search')]")


copy_poi_tag_clickable = "//poi-input//button/span[contains(., 'Copy')]"


def copy_poi_tag_button(self):
    return find_element_xpath(self,
                              "//poi-input//button/span[contains(., 'Copy')]")


cancel_poi_tag_clickable = "//poi-input/div/resize-drag//button/span[contains(., 'Cancel')]"


def cancel_poi_tag_button(self):
    return find_element_xpath(self, "//poi-input/div/resize-drag//button/span[contains(., 'Cancel')]")


ok_poi_tag_clickable = "//poi-input/div/resize-drag//button/span[contains(., 'Ok')]"


def ok_poi_tag_button(self):
    return find_element_xpath(self, "//poi-input/div/resize-drag//button/span[contains(., 'Ok')]")


def input_poi_tag(self):
    return find_element_xpath(self,
                              "//poi-input/div/resize-drag//mat-form-field[1]//input[@formcontrolname='name']")


# First person view
alignment_clickable = "//switch-poi-mode//mat-icon[@data-mat-icon-name='align-poi']"


def alignment_button(self):
    return find_element_xpath(self, "//switch-poi-mode//mat-icon[@data-mat-icon-name='align-poi']")


manual_alignment_clickable = "//alignment-menu-content//button/span[contains(., 'Manual alignment')]"


def manual_alignment_button(self):
    return find_element_xpath(self,
                              "//alignment-menu-content/mat-card/mat-card-content/div/button/span[contains(., 'Manual alignment')]")


restore_original_alignment = "//alignment-menu-content//button/span[contains(., 'Restore original alignment')]"


def restore_original_alignment_button(self):
    return find_element_xpath(self,
                              "//alignment-menu-content/mat-card/mat-card-content/div/button/span[contains(., 'Restore original alignment')]")


poi_type_menu_clickable = "//app-root//poi-inter//mat-select[@role='combobox']"


def poi_type_menu_button(self):
    return find_element_xpath(self, "//app-root//poi-inter//mat-select[@role='combobox']")


poi_name_input_clickable = "//controls//poi-input//input[@formcontrolname='name']"


def poi_name_input(self):
    return find_element_xpath(self, "//controls//poi-input//input[@formcontrolname='name']")


save_edit_poi_button_clickable = "//poi-input//button/span[contains(., 'Save')]"


def save_edit_poi_button(self):
    return find_element_xpath(self, "//poi-input//button/span[contains(., 'Save')]")


# /html/body/div[4]/div[4]/div/mat-dialog-container/create-edit-poi-type/form/mat-dialog-actions/section[2]/button[2]
discard_edit_poi_button_clickable = "//poi-input//button/span[contains(., 'Discard')]"


def discard_edit_poi_button(self):
    return find_element_xpath(self, "//poi-input//button/span[contains(., 'Discard')]")


# Multimedia:

close_multimedia_button_clickable = "//multimedia-dialog//button//mat-icon[@data-mat-icon-name='cancel']"


def close_multimedia_button(self):
    return find_element_xpath(self, "//multimedia-dialog//button//mat-icon[@data-mat-icon-name='cancel']")


save_multimedia_button_clickable = "//multimedia-dialog//button/span[contains(., 'Save')]"


def save_multimedia_button(self):
    return find_element_xpath(self, "//multimedia-dialog//button/span[contains(., 'Save')]")


discard_multimedia_button_clickable = "//multimedia-dialog//button/span[contains(., 'Discard')]"


def discard_multimedia_button(self):
    return find_element_xpath(self, "//multimedia-dialog//button/span[contains(., 'Discard')]")


ok_multimedia_button_clickable = "//multimedia-dialog//button/span[contains(., 'Ok')]"


def ok_multimedia_button(self):
    return find_element_xpath(self, "//multimedia-dialog//button/span[contains(., 'Ok')]")


edit_multimedia_clickable = "//multimedia-dialog//input-file//mat-icon[@data-mat-icon-name='edit']"


def edit_multimedia_button(self):
    return find_element_xpath(self, "//multimedia-dialog//input-file//mat-icon[@data-mat-icon-name='edit']")


minimize_multimedia_clickable = "//multimedia-dialog//button//mat-icon[@data-mat-icon-name='screen-browser']"


def minimize_multimedia_button(self):
    return find_element_xpath(self,
                              "//multimedia-dialog//button//mat-icon[@data-mat-icon-name='screen-browser']")


# POI menu titles:
point_of_interest_title = "//mat-card-header/div/mat-card-title/span[contains(., 'Point of interest')]"
edit_point_of_interest_title = "//mat-card-header/div/mat-card-title/span[contains(., 'Edit point of interest')]"
create_point_of_interest_title = "//mat-card-header/div/mat-card-title/span[contains(., 'Create point of interest')]"
edit_poi_type_title = "//create-edit-poi-type/dialog-title/div/h1[contains(., 'Edit type')]"
delete_selected_items_title = "//confirm-dialog/dialog-title/div/h1[contains(., 'Delete selected items')]"
delete_attribute_title = "//mat-dialog-container/confirm-dialog/dialog-title/div/h1[contains(., 'Delete attribute')]"

# Regions buttons:
########################################################################################################################
region_name_input = "//controls/div//extract-inter//input[@data-placeholder='Region name']"

start_selection_clickable = "//extract-inter/mat-card/form/mat-card-content" \
                            "/mat-button-toggle[@role='presentation']/button"


def start_selection_button(self):
    return find_element_xpath(self,
                              "//extract-inter/mat-card/form/mat-card-content/mat-button-toggle[@role='presentation']/button")


confirm_region_clickable = "//extract-content/confirm-buttons//button-fab[contains(@class, 'button-apply')]/div/button"


def confirm_region_button(self):
    return find_element_xpath(self,
                              "//extract-content/confirm-buttons/div/button-fab[contains(@class, 'button-apply')]/div/button")


cancel_region_clickable = "//extract-content/confirm-buttons//button-fab[contains(@class, 'button-cancel')]/div/button"


def cancel_region_button(self):
    return find_element_xpath(self,
                              "//extract-content/confirm-buttons//button-fab[contains(@class, 'button-cancel')]/div/button")


edit_region_name_cancel_clickable = "//region-edit-name//button/span[contains(., 'Cancel')]"


def edit_region_name_cancel_button(self):
    return find_element_xpath(self,
                              "//region-edit-name//button/span[contains(., 'Cancel')]")


edit_region_name_ok_clickable = "//region-edit-name//button/span[contains(., 'Ok')]"


def edit_region_name_ok_button(self):
    return find_element_xpath(self,
                              "//region-edit-name//button/span[contains(., 'Ok')]")


edit_region_name_input = "//region-edit-name//input[@name='region-name']"

regions_tag_add_clickable = "//button//mat-icon[@data-mat-icon-name='add']"


def regions_tag_add_button(self):
    return find_element_xpath(self,
                              "//button//mat-icon[@data-mat-icon-name='add']")


regions_tag_cancel_clickable = "//tags//button//span[contains(., 'Cancel')]"


def regions_tag_cancel_button(self):
    return find_element_xpath(self, "//tags//button//span[contains(., 'Cancel')]")


regions_tag_ok_clickable = "//tags//button//span[contains(., 'Ok')]"


def regions_tag_ok_button(self):
    return find_element_xpath(self, "//tags//button//span[contains(., 'Ok')]")


regions_export_cancel_clickable = "//export-region//button/span[contains(., 'Cancel')]"


def regions_export_cancel_button(self):
    return find_element_xpath(self, "//export-region//button/span[contains(., 'Cancel')]")


regions_export_apply_clickable = "//export-region//button/span[contains(., 'Export')]"


def regions_export_apply_button(self):
    return find_element_xpath(self, "//export-region//button/span[contains(., 'Export')]")


regions_export_name_input = "//export-region//input[@formcontrolname='name']"
regions_export_ptx_format = "//mat-option/span[contains(., '.ptx')]"
regions_export_e57_format = "//mat-option/span[contains(., '.e57')]"

regions_export_arrow_clickable = "//export-region//mat-form-field[2]//mat-select/div/div[2]"


def regions_export_arrow_button(self):
    return find_element_xpath(self, "//export-region//mat-form-field[2]//mat-select/div/div[2]")


copy_the_selected_area_clickable = "//extract-inter//span[contains(., 'Сopy the selected area')]/../../" \
                                   "span[@class='mat-slide-toggle-bar']"


def copy_the_selected_area_button(self):
    return find_element_xpath(self,
                              "//extract-inter//span[contains(., 'Сopy the selected area')]/../../span[@class='mat-slide-toggle-bar']")


def regions_tag_input(self):
    return find_element_xpath(self, "//tags//input[@formcontrolname='name']")


# Clash Detection buttons:
########################################################################################################################
clash_start_clickable = "//clash-detection-inter//button-raised/button[contains(., 'Start')]"


def clash_start_button(self):
    return find_element_xpath(self,
                              "//clash-detection-inter//button-raised/button[contains(., 'Start')]")


clash_dynamic_clickable = "//clash-detection-inter/div/mat-card//button/span[contains(., 'Dynamic')]"


def clash_dynamic_button(self):
    return find_element_xpath(self,
                              "//clash-detection-inter/div/mat-card//button/span[contains(., 'Dynamic')]")


clash_detailed_clickable = "//clash-detection-inter/div/mat-card//button/span[contains(., 'Detailed')]"


def clash_detailed_button(self):
    return find_element_xpath(self,
                              "//clash-detection-inter/div/mat-card//button/span[contains(., 'Detailed')]")


def choose_objects_to_detect(self, selected_only=None, all=None):
    find_element_xpath(self, "//clash-detection-inter/div/mat-card//mat-select[@role='combobox']").click()
    time.sleep(1)
    if selected_only:
        find_element_xpath(self, "//span[contains(., 'Selected only')]").click()
    if all:
        find_element_xpath(self, "//span[contains(., 'All')]").click()


# Move and rotate buttons:
########################################################################################################################

manipulator_apply_clickable = "//numpad//div[contains(@class, 'accept-control')]/mat-icon[@data-mat-icon-name='apply']"


def manipulator_apply_button(self):
    return find_element_xpath(self,
                              "//numpad//div[contains(@class, 'accept-control')]/mat-icon[@data-mat-icon-name='apply']")


def manipulator_point(self):
    return find_element_xpath(self,
                              "//numpad/mat-card/mat-card-content/div/div/div/div[contains(., '.')]")


def manipulator_cleanup(self):
    return find_element_xpath(self,
                              "//numpad/mat-card/mat-card-content/div/div/div/div[contains(., 'C')]")


def manipulator_minus(self):
    return find_element_xpath(self,
                              "//numpad/mat-card/mat-card-content/div/div/div/div/mat-icon[@data-mat-icon-name='minus']")


def manipulator_backspace(self):
    return find_element_xpath(self,
                              "//numpad/mat-card/mat-card-content/div/div/div/div/mat-icon[@data-mat-icon-name='backspace']")


move_and_rotate_copy_clickable = "//controls//interpanel/transform-inter//button-raised/button[contains(., 'Copy')]"


def move_and_rotate_copy_button(self):
    return find_element_xpath(self,
                              "//controls//interpanel/transform-inter//button-raised/button[contains(., 'Copy')]")


confirm_move_and_rotate_clickable = "//confirm-buttons//button-fab[contains(@class, 'button-apply')]/div/button"


def confirm_move_and_rotate_button(self):
    return find_element_xpath(self,
                              "//confirm-buttons//button-fab[contains(@class, 'button-apply')]/div/button")


cancel_move_and_rotate_clickable = "//transform-content/confirm-buttons/" \
                                   "/button-fab[contains(@class, 'button-cancel')]/div/button"


def cancel_move_and_rotate_button(self):
    return find_element_xpath(self,
                              "//transform-content/confirm-buttons//button-fab[contains(@class, 'button-cancel')]/div/button")


global_coordinates_switch_clickable = "//transform-inter//mat-slide-toggle/label/span[@class='mat-slide-toggle-bar']"


def global_coordinates_switch_button(self):
    return find_element_xpath(self,
                              "//transform-inter//mat-slide-toggle/label/span[@class='mat-slide-toggle-bar']")


# Placement buttons:
########################################################################################################################

confirm_placement_clickable = "//place-content/confirm-buttons//button-fab[contains(@class, 'button-apply')]/div/button"


def confirm_placement_button(self):
    return find_element_xpath(self,
                              "//place-content/confirm-buttons//button-fab[contains(@class, 'button-apply')]/div/button")


cancel_placement_clickable = "//place-content/confirm-buttons//button-fab[contains(@class, 'button-cancel')]/div/button"


def cancel_placement_button(self):
    return find_element_xpath(self,
                              "//place-content/confirm-buttons//button-fab[contains(@class, 'button-cancel')]/div/button")


placement_orient_clickable = "//place-inter/div/mat-card//div/button//mat-icon[@data-mat-icon-name='placement-axes']"


def placement_orient_button(self):
    return find_element_xpath(self,
                              "//place-inter/div/mat-card//div/button//mat-icon[@data-mat-icon-name='placement-axes']")


placement_glue_clickable = "//place-inter/div/mat-card//div/button//mat-icon[@data-mat-icon-name='placement-planes']"


def placement_glue_button(self):
    return find_element_xpath(self,
                              "//place-inter/div/mat-card//div/button//mat-icon[@data-mat-icon-name='placement-planes']")


placement_mirror_clickable = "//place-inter/div/mat-card//button/span[contains(., 'Mirror')]"


def placement_mirror_button(self):
    return find_element_xpath(self, "//place-inter/div/mat-card//button/span[contains(., 'Mirror')]")


# Clipping box buttons:
########################################################################################################################

clipping_box_reset_clickable = "//controls//lobby//button-basic[@innerlabel='CLIPBOX_RESET_TO_DEFAULT']"


def clipping_box_reset_button(self):
    return find_element_xpath(self, "//controls//lobby//button-basic[@innerlabel='CLIPBOX_RESET_TO_DEFAULT']")


clipping_box_expand_options_clickable = "//controls//lobby//button//mat-icon[@data-mat-icon-name='scenario-list']"


def clipping_box_expand_options_button(self):
    return find_element_xpath(self, "//controls//lobby//button//mat-icon[@data-mat-icon-name='scenario-list']")


clipping_box_highlight_switch_clickable = "//mat-card-content//mat-slide-toggle/label//" \
                                          "span[contains(., 'Highlight Edges')]/../span[@class='mat-slide-toggle-bar']"


def clipping_box_highlight_switch(self):
    return find_element_xpath(self,
                              "//mat-card-content//mat-slide-toggle/label//span[contains(., 'Highlight Edges')]"
                              "/../span[@class='mat-slide-toggle-bar']")


clipping_box_capping_switch_clickable = "//mat-card-content//mat-slide-toggle/label//span[contains(., 'Capping')]" \
                                        "/../span[@class='mat-slide-toggle-bar']"


def clipping_box_capping_switch(self):
    return find_element_xpath(self, "//mat-card-content//mat-slide-toggle/label//span[contains(., 'Capping')]"
                                    "/../span[@class='mat-slide-toggle-bar']")


clipping_box_invert_switch_clickable = "//mat-card-content//mat-slide-toggle/label//span[contains(., 'Invert')]" \
                                       "/../span[@class='mat-slide-toggle-bar']"


def clipping_box_invert_switch(self):
    return find_element_xpath(self, "//mat-card-content//mat-slide-toggle/label//span[contains(., 'Invert')]"
                                    "/../span[@class='mat-slide-toggle-bar']")


clipping_box_apply_clickable = "//controls//lobby/div/mat-card/mat-card-content/div/button-mini-fab[@icon='apply']"


def clipping_box_apply_button(self):
    return find_element_xpath(self,
                              "//controls//lobby/div/mat-card/mat-card-content/div/button-mini-fab[@icon='apply']")


clipping_box_cancel_clickable = "//controls//lobby/div/mat-card/mat-card-content/div/button-mini-fab[@icon='cancel']"


def clipping_box_cancel_button(self):
    return find_element_xpath(self,
                              "//controls//lobby/div/mat-card/mat-card-content/div/button-mini-fab[@icon='cancel']")


# Custom view buttons:

def edit_view_name_input(self):
    return find_element_xpath(self, "//edit-dialog/mat-dialog-content//input[@type='text']")


edit_view_name_ok_clickable = "//edit-dialog//button/span[contains(., 'Ok')]"


def edit_view_name_ok_button(self):
    return find_element_xpath(self, "//edit-dialog//button/span[contains(., 'Ok')]")


edit_view_name_cancel_clickable = "//edit-dialog//button/span[contains(., 'Cancel')]"


def edit_view_name_cancel_button(self):
    return find_element_xpath(self, "//edit-dialog//button/span[contains(., 'Cancel')]")


edit_view_name_close_clickable = "//edit-dialog//button//mat-icon[@data-mat-icon-name='cancel']"


def edit_view_name_close_button(self):
    return find_element_xpath(self, "//edit-dialog//button//mat-icon[@data-mat-icon-name='cancel']")


create_view_save_clickable = "//lobby/div/mat-card/mat-card-actions//button[contains(., 'SAVE')]"


def create_view_save_button(self):
    return find_element_xpath(self,
                              "//lobby/div/mat-card/mat-card-actions//button[contains(., 'SAVE')]")


create_view_cancel_clickable = "//lobby/div/mat-card/mat-card-actions//button[contains(., 'Cancel')]"


def create_view_cancel_button(self):
    return find_element_xpath(self, "//lobby/div/mat-card/mat-card-actions//button[contains(., 'Cancel')]")


create_view_name_input_available = "//lobby/div/mat-card/mat-card-content//input[@formcontrolname='name']"


def create_view_name_input(self):
    return find_element_xpath(self, "//lobby/div/mat-card/mat-card-content//input[@formcontrolname='name']")


# Scenario buttons:
########################################################################################################################
scenario_record_clickable = "//mat-drawer-content[@id='viewer3d']/controls/div/div[3]/div/lobby/div/mat-card/mat-card-content/div/button-fab/div/button"

scenario_pause_clickable = "//mat-drawer-content[@id='viewer3d']/controls/div/div[3]/div/lobby/div/mat-card/mat-card-content/div/button-fab[contains (@class, 'button-pause')]/div/button"
scenario_appy_clickable = "//lobby/div/mat-card/mat-card-content/div/button-mini-fab[1]/button"
# scenario_name_input_available = "//input[@placeholder='Scenario name']"
scenario_name_input_available = "/html/body/div[1]/app-root/workbench/div/div/project-viewer/viewer-ui/mat-drawer-container/mat-drawer-content/controls/div/div[4]/div/lobby/div/mat-card/mat-card-content/div/form/mat-form-field/div/div[1]/div/input"
scenario_save_clickable = "//lobby/div/mat-card/mat-card-actions/button-raised/button"
scenario_list_clickable = "//lobby/div/mat-card/mat-card-content/div/button-mini-fab/button"
scenario_play_clickable = "//lobby/div/mat-card/mat-card-content/mat-grid-list/div/mat-grid-tile/figure/div/div/button-mini-fab/button"
scenario_delete_confirm_clickable = "//mat-card/mat-card-actions/button-basic/button/span[span[contains(., 'DELETE')]]/.."
scenario_close_clickable = "//mat-drawer-content[@id='viewer3d']/controls/div/div[3]/div/lobby/div/mat-card/mat-card-content/div/button-mini-fab[@icon='cancel']/button"


def scenario_record_button(self):
    return find_element_xpath(self,
                              "//controls//lobby/div/mat-card//button//mat-icon[@data-mat-icon-name='record']")


def scenario_pause_button(self):
    return find_element_xpath(self,
                              "//controls//lobby/div/mat-card//button//mat-icon[@data-mat-icon-name='pause']")


def scenario_apply_button(self):
    return find_element_xpath(self,
                              "//controls//lobby/div/mat-card//button-mini-fab[@icon='apply']")


def scenario_cancel_button(self):
    return find_element_xpath(self,
                              "//controls//lobby/div/mat-card//button-mini-fab[@icon='cancel']")


def scenario_name_input(self):
    return find_element_xpath(self,
                              "//lobby/div/mat-card//input[@formcontrolname='name']")


def fill_in_the_scenario_name(self, scenario_name: str) -> None:
    scenario_name_input(self).send_keys(f"{scenario_name}")


def scenario_save(self):
    return find_element_xpath(self, "//lobby/div/mat-card/mat-card-actions//button[contains(., 'SAVE')]")


def confirm_scenario_delete(self):
    return find_element_xpath(self,
                              "//mat-card/mat-card-actions/button-basic/button/span[contains(., 'DELETE')]")


def cancel_scenario_delete(self):
    return find_element_xpath(self,
                              "//mat-card/mat-card-actions/button-raised/button/span[contains(., 'Cancel')]")


def scenario_list(self):
    return find_element_xpath(self, "//lobby/div/mat-card/mat-card-content/div/button-mini-fab/button")


def scenario_play(self):
    return find_element_xpath(self,
                              "//lobby/div/mat-card/mat-card-content/mat-grid-list/div/mat-grid-tile/figure/div/div/button-mini-fab/button")


def scenario_pause_play(self):
    return find_element_xpath(self,
                              "//lobby/div/mat-card/mat-card-content/mat-grid-list/div/mat-grid-tile/figure/div/div/button-mini-fab/button")


def scenario_list_close(self):
    return find_element_xpath(self,
                              "//lobby/div/mat-card/mat-card-header/div/mat-card-title/button-icon/div/button")


def scenario_input_bar(self):
    return find_element_xpath(self,
                              "//lobby/div/mat-card/mat-card-content/div/form/mat-form-field/div/div[1]/div/input")


# Domain buttons:
########################################################################################################################
activated_domain_label = '//mat-toolbar/mat-toolbar-row//span[@class="mat-tooltip-trigger activated-domain-label"]'
PMI_items_counter = '//viewermenu/div/div/p[@class="mat-body viewermenu-select-counter ng-star-inserted"]'
domain_checkbox_point_clouds = '//mat-checkbox[@formcontrolname="pointClouds"]'
domain_checkbox_3d_models = '//mat-checkbox[@formcontrolname="cadModels"]'
subdomain_checkbox_point_clouds = '//mat-checkbox[@formcontrolname="pointClouds"]'
subdomain_checkbox_3d_models = '//mat-checkbox[@formcontrolname="cadModels"]'

exit_the_domain_clickable = '//mat-toolbar-row/div/div/div/button-icon[@class="cancelActivatedDomain ng-star-inserted"]'


def exit_the_domain_button(self):
    return find_element_xpath(self,
                              '//mat-toolbar-row/div/div/div/button-icon[@class="cancelActivatedDomain ng-star-inserted"]')


domain_name_input_clickable = "//domain-dialog/form/mat-dialog-content/mat-form-field/div/div/div/input"


def domain_name_input(self):
    return find_element_xpath(self, "//domain-dialog/form/mat-dialog-content/mat-form-field/div/div/div/input")


domain_create_clickable = "//domain-dialog/form/mat-dialog-actions/button[@type='submit']"


def domain_create_button(self):
    return find_element_xpath(self, "//domain-dialog/form/mat-dialog-actions/button[@type='submit']")


domain_list_clickable = "//domains-tree//mat-expansion-panel-header//span[contains(., 'Domains')]/../" \
                        "mat-icon"


def domain_list_button(self):
    return find_element_xpath(self, "//domains-tree//mat-expansion-panel-header//span"
                                    "[contains(., 'Domains')]/../mat-icon")


domain_arrow_clickable = '//cdk-virtual-scroll-viewport/div/div[1]/div//mat-icon[@data-mat-icon-name="apply"]'


def domain_arrow_button(self):
    return find_element_xpath(self,
                              '//cdk-virtual-scroll-viewport/div/div[1]/div//mat-icon[@data-mat-icon-name="apply"]')


subdomain_arrow_clickable = '//cdk-virtual-scroll-viewport/div/div[2]/div//mat-icon[@data-mat-icon-name="apply"]'


def subdomain_arrow_button(self):
    return find_element_xpath(self,
                              '//cdk-virtual-scroll-viewport/div/div[2]/div//mat-icon[@data-mat-icon-name="apply"]')


assembly_list_clickable = "//assembly-content//project-tree//button[@aria-label='toggle undefined']"


def assembly_list(self):
    return find_element_xpath(self,
                              "//assembly-content//project-tree//button[@aria-label='toggle undefined']")


etp_name_input_clickable = '//export-domain-dialog/mat-dialog-content/div//input[@data-placeholder="Project name"]'


def etp_name_input(self):
    return find_element_xpath(self,
                              '//export-domain-dialog/mat-dialog-content/div//input[@data-placeholder="Project name"]')


etf_input_clickable = '//export-domain-dialog/mat-dialog-content/div//input[@data-placeholder="File name"]'


def etf_name_input(self):
    return find_element_xpath(self,
                              '//export-domain-dialog/mat-dialog-content/div//input[@data-placeholder="File name"]')


export_ok_button_clickable = "//mat-dialog-actions/div/button-raised/button/span[contains(., 'Ok')]"


def export_ok_button(self):
    return find_element_xpath(self,
                              "//mat-dialog-actions/div/button-raised/button/span[contains(., 'Ok')]")


subdomain_list_clickable = "//domains-tree//project-tree//button[@aria-label='toggle undefined']"


def subdomain_list_button(self):
    return find_element_xpath(self,
                              "//domains-tree//project-tree//button[@aria-label='toggle undefined']")


task_cancel_clickable = '//mat-cell/button-icon/div/button/span/mat-icon[@data-mat-icon-name="cancel"]'


def task_cancel_button(self, job_parameter: str):
    return find_element_xpath(self, f"//upload-tasks//span[contains(., '{job_parameter}')]/../.."
                                    f"//button/span/mat-icon[@data-mat-icon-name='cancel']")


task_remove_clickable = '//mat-cell/button-icon/div/button/span/mat-icon[@data-mat-icon-name="remove"]'


def task_remove_button(self, job_parameter: str):
    return find_element_xpath(self, f"//upload-tasks//span[contains(., '{job_parameter}')]/../.."
                                    f"//button/span/mat-icon[@data-mat-icon-name='remove']")


define_a_domain_list_clickable = '//mat-form-field//button/span/mat-icon[@data-mat-icon-name="expand_more"]'


def define_a_domain_list_button(self):
    return find_element_xpath(self,
                              '//mat-form-field//button/span/mat-icon[@data-mat-icon-name="expand_more"]')


export_domain_cross_button_clickable = '//mat-form-field/div/div/div/button-icon/div/button/' \
                                       'span/mat-icon[@data-mat-icon-name="cancel"]'


def export_domain_cross_button(self):
    return find_element_xpath(self,
                              '//mat-form-field/div/div/div/button-icon/div/button/span/mat-icon[@data-mat-icon-name="cancel"]')


download_file_clickable = "//mat-cell/button-icon/div/button/span/mat-icon[@data-mat-icon-name='download']"


def download_file_button(self, job_parameter: str):
    return find_element_xpath(self, f"//upload-tasks//span[contains(., '{job_parameter}')]/../.."
                                    f"//button/span/mat-icon[@data-mat-icon-name='download']")


# Group of items
########################################################################################################################

discard_groups_clickable = "//group-dialog/form/mat-dialog-actions/div/button[contains(., 'Discard')]"


def discard_groups_button(self):
    return find_element_xpath(self,
                              "//group-dialog/form/mat-dialog-actions/div/button[contains(., 'Discard')]")


groups_name_input_clickable = "//mat-dialog-container/group-dialog//input[@formcontrolname='name']"


def groups_name_input(self):
    return find_element_xpath(self,
                              "//mat-dialog-container/group-dialog//input[@formcontrolname='name']")


create_group_clickable = "//group-dialog/form/mat-dialog-actions/div/button[contains(., 'Create')]"


def create_group_button(self):
    return find_element_xpath(self,
                              "//group-dialog/form/mat-dialog-actions/div/button[contains(., 'Create')]")


select_group_combobox = "//group-dialog//mat-icon[@data-mat-icon-name='expand_more']"


def select_group_combobox_button(self):
    return find_element_xpath(self,
                              "//group-dialog//mat-icon[@data-mat-icon-name='expand_more']")


save_group_clickable = "//group-dialog/form/mat-dialog-actions/div/button[contains(., 'Save')]"


def save_group_button(self):
    return find_element_xpath(self,
                              "//group-dialog/form/mat-dialog-actions/div/button[contains(., 'Save')]")


remove_item_cancel_clickable = "//confirm-dialog/mat-dialog-actions/div/button-basic/button[contains(., 'Cancel')]"


def remove_item_cancel_button(self):
    return find_element_xpath(self,
                              "//confirm-dialog/mat-dialog-actions/div/button-basic/button[contains(., 'Cancel')]")


remove_item_clickable = "//confirm-dialog/mat-dialog-actions/div/button-raised/button[contains(., 'Remove')]"


def remove_item_button(self):
    return find_element_xpath(self,
                              "//confirm-dialog/mat-dialog-actions/div/button-raised/button[contains(., 'Remove')]")


remove_group_clickable = "//confirm-dialog/mat-dialog-actions/div/button-raised/button[contains(., 'Delete')]"


def remove_group_button(self):
    return find_element_xpath(self,
                              "//confirm-dialog/mat-dialog-actions/div/button-raised/button[contains(., 'Delete')]")


three_dots_icon_clickable = "//project-tree//button/span/mat-icon[@data-mat-icon-name='more-vertical']"


def three_dots_icon_button(self):
    return find_element_xpath(self,
                              "//project-tree//button/span/mat-icon[@data-mat-icon-name='more-vertical']")


delete_group_clickable = "//button[contains(., 'Delete')]"


def delete_group_button(self):
    return find_element_xpath(self, "//button[contains(., 'Delete')]")


settings_group_clickable = "//button[contains(., 'Settings')]"


def settings_group_button(self):
    return find_element_xpath(self, "//button[contains(., 'Settings')]")


tags_group_clickable = "//button[contains(., 'Tags')]"


def tags_group_button(self):
    return find_element_xpath(self, "//button[contains(., 'Tags')]")


color_clickable = "/html/body/div[3]/div[2]/div/mat-dialog-container/group-dialog/form/mat-dialog-content/div/div/input"


def color_button(self):
    return find_element_xpath(self,
                              "/html/body/div[3]/div[2]/div/mat-dialog-container/group-dialog/form/mat-dialog-content/div/div/input")


# Tags buttons:
########################################################################################################################

add_tag_clickable = "//tags//button//mat-icon[@data-mat-icon-name='add']"


def add_tag_button(self):
    return find_element_xpath(self, "//tags//button//mat-icon[@data-mat-icon-name='add']")


tag_input_available = "//tags//input[@formcontrolname='name']"


def tag_input(self):
    return find_element_xpath(self, "//tags//input[@formcontrolname='name']")


tag_ok_clickable = "//tags//button/span[contains(., 'Ok')]"


def tag_ok_button(self):
    return find_element_xpath(self, "//tags//button/span[contains(., 'Ok')]")


tag_cancel_clickable = "//tags//button/span[contains(., 'Cancel')]"


def tag_cancel_button(self):
    return find_element_xpath(self, "//tags//button/span[contains(., 'Cancel')]")


close_tag_clickable = "//tags//mat-icon[@data-mat-icon-name='cancel']"


def close_tag_button(self):
    return find_element_xpath(self,
                              "//tags//mat-icon[@data-mat-icon-name='cancel']")


tag_edit_clickable = "//tags//button/span[contains(., 'Edit')]"


def tag_edit_button(self):
    return find_element_xpath(self, "//tags//button/span[contains(., 'Edit')]")


tag_delete_clickable = "//tags//button/span[contains(., 'Delete')]"


def tag_delete_button(self):
    return find_element_xpath(self, "//tags//button/span[contains(., 'Delete')]")


tag_copy_clickable = "//tags//button/span[contains(., 'Copy')]"


def tag_copy_button(self):
    return find_element_xpath(self, "//tags//button/span[contains(., 'Copy')]")


tag_search_clickable = "//tags//button/span[contains(., 'Search')]"


def tag_search_button(self):
    return find_element_xpath(self, "//tags//button/span[contains(., 'Search')]")


def tags_list(self):
    return find_elements_by_xpath(self, "//tags//mat-chip-list/div/mat-chip")


def tags_title(self):
    return find_element_xpath(self, "//span[contains(., '# Tags')]")


def tag_string(self):
    return find_element_xpath(self,
                              "//tags/resize-drag/mat-card/div/div/div/div/mat-chip-list/div/mat-chip/span")


########################################################################################################################


def scenario_record_timer(self):
    return find_element_xpath(self, "//lobby/div/mat-card/mat-card-content/div/div/timer/span")


def search_place(self):
    return find_element_xpath(self,
                              "//mat-option[contains(@class,'mat-search-option')]/span/mat-row/mat-cell[contains(@class,'mat-column-position')]/button-icon/div/button")


def search_placeIt(self, name):
    find_element_xpath(self,
                       "//mat-option[contains(.,name)]/span/mat-row/mat-cell[contains(@class,'mat-column-position')]/button-icon/div/button").click()


def search_show(self):
    return find_element_xpath(self,
                              "//mat-option[contains(@class,'mat-search-option')]/span/mat-row/mat-cell[contains(@class,'mat-column-visible')]/button-icon/div/button")


def search_remove_one(self):
    return find_element_xpath(self,
                              "//mat-option[contains(@class,'mat-search-option')]/span/mat-row/mat-cell[contains(@class,'mat-column-remove')]/button-icon/div/button")


def expand_menu(self):
    return find_element_xpath(self,
                              "//div[@class='project-menu']/button-icon[@icon='more-vertical']/div/button")


def add_files_button(self):
    return find_element_xpath(self,
                              "//div[@class='project-actions']/button-basic[@innerlabel='PROJECT_UPLOAD']/button")


def load_fll_model(self):
    return find_element_xpath(self, "//div[3]/div[2]/button-icon[1]/div/button")


# def annotation_textarea(self, text):
def fill_textarea(self, element, text):
    element(self).send_keys(Keys.CONTROL + "a")
    element(self).send_keys(Keys.DELETE)
    element(self).send_keys(text)
    time.sleep(1)
    return


def is_image_changed(self, im1, im2, mes):
    try:
        mc, sc, diff = compare_images(im1, im2, mes)
        res = (mc < 10) and (sc > 0.95)
        self.assertTrue(res, mes)
        print("<table width='100%'><tr>")
        print(
            "<td height='300px' width='533px'><a target='_blank' href='./" + im2 + "'><img src='./" + im2 + "' height='300px' width='533px'/></a></td>")
        print(
            "<td height='300px' width='533px'><a target='_blank' href='./" + im1 + "'><img src='./" + im1 + "' height='300px' width='533px'/></a></td>")
        print(
            "<td height='300px' width='533px'><a target='_blank' href='./" + diff + "'><img src='./" + diff + "' height='300px' width='533px'/></a></td>")
        print("</tr></table>")
        return False
    except AssertionError as e:
        print(mes + "screenshot is changed")
        self.verificationErrors.append(str(e))
        print("<table width='100%'><tr>")
        print(
            "<td height='300px' width='533px'><a target='_blank' href='./" + im2 + "'><img src='./" + im2 + "' height='300px' width='533px'/></a></td>")
        print(
            "<td height='300px' width='533px'><a target='_blank' href='./" + im1 + "'><img src='./" + im1 + "' height='300px' width='533px'/></a></td>")
        print(
            "<td height='300px' width='533px'><a target='_blank' href='./" + diff + "'><img src='./" + diff + "' height='300px' width='533px'/></a></td>")
        print("</tr></table>")
        return True


def is_element_present(self, how, what):
    try:
        self.driver.find_element(by=how, value=what)
    except NoSuchElementException as e:
        return False
    return True


def is_element_displayed(self, elem):
    elemList = find_elements_by_xpath(self, elem)
    if len(elemList) < 1:
        return False
    else:
        if elemList[0].is_displayed():
            return True
        else:
            return False


def is_alert_present(self):
    try:
        self.driver.switch_to_alert()
    except NoAlertPresentException as e:
        return False
    return True


def close_alert_and_get_its_text(self):
    try:
        alert = self.driver.switch_to_alert()
        alert_text = alert.text
        if self.accept_next_alert:
            alert.accept()
        else:
            alert.dismiss()
        return alert_text
    finally:
        self.accept_next_alert = True


def tear_down(self):
    self.driver.quit()


# def log_parse(self):
#     browser_choice = os.getenv("BROWSER_CHOICE")
#     if browser_choice == "Firefox":
#         self.assertEqual([], self.verificationErrors)
#     else:
#         logs = []
#         logs = self.driver.get_log('browser')
#         for entry in logs:
#             if "OCC_ERROR: Not supported:" not in str(entry):
#                 if "\"ERROR\" Error: Uncaught (in promise): InvalidStateError: An attempt was made to use an object that is not, or is no longer, usable." not in str(
#                         entry):
#                     if "Failed to load resource: the server responded with a status of 500" not in str(entry):
#                         if "Backend returned code 500, body was" not in str(entry):
#                             if "'https://adservice.google.com/adsid/google/ui - Failed to load resource: net::ERR_CONNECTION_RESET" not in str(
#                                     entry):
#                                 if "WebSocket connection to" not in str(entry):
#                                     if "Failed to load resource: the server responded with a status of 422 ()" not in str(
#                                             entry):
#                                         if "\"ERROR\" Me" not in str(entry):
#                                             if "Failed to load resource: net::ERR_FAILED" not in str(entry):
#                                                 if entry['level'] == 'SEVERE':
#                                                     print('Browser Error: ' + str(entry))
#                                                     self.verificationErrors.append('Browser error: ' + str(entry))
#         self.assertEqual([], self.verificationErrors)


def log_parse_login(self):
    logs = []
    logs = self.driver.get_log('browser')
    for entry in logs:
        if "OCC_ERROR: Not supported:" not in str(entry):
            if "the server responded with a status of 401" not in str(entry):
                if entry['level'] == 'SEVERE':
                    print('Browser Error: ' + str(entry))
                    self.verificationErrors.append('Browser error: ' + str(entry))

    # self.assertEqual([], self.verificationErrors)
    # assert [] == self.verificationErrors, message + f"{current} not equal {reference}"


def log_parse_perf(self):
    logs = []
    logs = self.driver.get_log('browser')
    for entry in logs:
        if "OCC_ERROR: Not supported:" not in str(entry):
            if "ws connection closed with error" not in str(entry):
                if entry['level'] == 'SEVERE':
                    print('Browser Error: ' + str(entry))
                    self.verificationErrors.append('Browser error: ' + str(entry))
    self.assertEqual([], self.verificationErrors)


def get_pid(self):
    # if isinstance(self.driver, webdriver.Chrome):
    process = psutil.Process(self.driver.service.process.pid)
    return process.children()[0].pid  # process id of browser tab
    # if isinstance(self.driver, webdriver.Firefox):
    #    return self.driver.service.process.pid       # process id of browser

    assert Exception('driver does not supported')


def upload_file(path_to_file, file_name):
    path = os.path.join(f'{path_to_file}', file_name)
    assert os.path.exists(path)

    # windows_shell = comclt.Dispatch("WScript.Shell")
    # time.sleep(2)
    # windows_shell.SendKeys(path)
    # time.sleep(2)
    # windows_shell.SendKeys("{TAB}")
    # time.sleep(2)
    # windows_shell.SendKeys("{TAB}")
    # time.sleep(2)
    # windows_shell.SendKeys("{ENTER}")
    # time.sleep(2)


def upload_file_from_dir(self, path_to_the_dir, *file_names):
    for file_name in file_names:
        choose_button(self).click()
        time.sleep(1)
        path = os.path.join(f'{path_to_the_dir}', file_name)
        assert os.path.exists(path)

        # windows_shell = comclt.Dispatch("WScript.Shell")
        # time.sleep(1)
        # windows_shell.SendKeys(path)
        # time.sleep(5)
        # windows_shell.SendKeys("{TAB}{TAB}")
        # time.sleep(1)
        # windows_shell.SendKeys("{ENTER}")
        # time.sleep(7)
        if len(file_names) > 1:
            clip_button(self).click()
            time.sleep(1)


def upload_file_as_link(self, *link_names):
    for i in range(0, len(link_names)):
        # Click on the clip icon
        clip_button(self).click()
        time.sleep(1)
        # Click on the link button
        wait_condition_clickable(self, link_button_clickable, time=15)
        link_button(self).click()
        time.sleep(1)
        # Enter the link
        url_address_input(self).send_keys(link_names[i])
        # Wait until ok button will be clickable
        wait_condition_clickable(self, ok_url_button_clickable, 15)
        ok_url_button(self).click()
        time.sleep(1)


def create_project_with_upload_from_dir(self, *file_names, project_name: str, path_to_the_dir: str,
                                        project_description=None,
                                        as_one_object=None, increase_rendering_efficiency=None,
                                        update_poi_position_and_poi_info=None,
                                        delete_poi_objects_and_recreate_them=None):
    # Click add or create new project
    if is_element_present(self, By.XPATH, add_project_clickable):
        add_project_button(self).click()
    else:
        create_new_project_button(self).click()
    time.sleep(1)
    # Fill in name field
    project_name_textarea(self).send_keys(project_name)
    time.sleep(1)
    # Fill in description field
    if project_description:
        project_name_textarea(self).click()
        project_name_textarea(self).send_keys(project_description)
    time.sleep(1)
    # Upload files
    upload_file_from_dir(self, path_to_the_dir, *file_names)
    # Parameters:
    if as_one_object:
        expand_upload_parameters(self).click()
        time.sleep(1)
        import_as_one_object_switch(self).click()
        time.sleep(1)
    if increase_rendering_efficiency:
        expand_upload_parameters(self).click()
        time.sleep(1)
        increase_rendering_efficiency_switch(self).click()
        time.sleep(1)
    # Actions on conflicting POI objects:
    if update_poi_position_and_poi_info:
        choose_actions_on_conflicting_poi_objects(self, update_position_and_poi_information=True)
    if delete_poi_objects_and_recreate_them:
        choose_actions_on_conflicting_poi_objects(self, delete_poi_objects_and_recreate_them=True)
    # Click create button
    create_project_button(self).click()
    time.sleep(2)


def create_project_with_upload_as_link(self, *link_names, project_name: str,
                                       project_description=None,
                                       as_one_object=None, increase_rendering_efficiency=None,
                                       update_poi_position_and_poi_info=None,
                                       delete_poi_objects_and_recreate_them=None):
    # Click add or create new project
    time.sleep(10)

    if is_element_present(self, By.XPATH, add_project_clickable):
        add_project_button(self).click()
    else:
        create_new_project_button(self).click()
    time.sleep(1)
    # Fill in name field
    project_name_textarea(self).send_keys(project_name)
    time.sleep(1)
    # Fill in description field
    if project_description:
        project_name_textarea(self).click()
        project_name_textarea(self).send_keys(project_description)
    time.sleep(1)
    # Upload files
    upload_file_as_link(self, *link_names)
    # Parameters:
    if as_one_object:
        expand_upload_parameters(self).click()
        time.sleep(1)
        import_as_one_object_switch(self).click()
        time.sleep(1)
    if increase_rendering_efficiency:
        expand_upload_parameters(self).click()
        time.sleep(1)
        increase_rendering_efficiency_switch(self).click()
        time.sleep(1)
    # Actions on conflicting POI objects:
    if update_poi_position_and_poi_info:
        choose_actions_on_conflicting_poi_objects(self, update_position_and_poi_information=True)
    if delete_poi_objects_and_recreate_them:
        choose_actions_on_conflicting_poi_objects(self, delete_poi_objects_and_recreate_them=True)
    # Click create button
    wait_condition_clickable(self, create_project_clickable, time=30)
    create_project_button(self).click()
    time.sleep(2)


def create_project_with_upload_as_link_and_custom_scale(self, *link_names, project_name: str, scale: list,
                                                        project_description=None,
                                                        as_one_object=None, increase_rendering_efficiency=None,
                                                        update_poi_position_and_poi_info=None,
                                                        delete_poi_objects_and_recreate_them=None):
    # Click add or create new project
    if is_element_present(self, By.XPATH, add_project_clickable):
        add_project_button(self).click()
    else:
        create_new_project_button(self).click()
    time.sleep(1)
    # Fill in name field
    project_name_textarea(self).send_keys(project_name)
    time.sleep(1)
    # Fill in description field
    if project_description:
        project_name_textarea(self).click()
        project_name_textarea(self).send_keys(project_description)
    time.sleep(1)
    # Upload files
    upload_file_as_link(self, *link_names)
    # Scale:
    for i in range(0, len(link_names)):
        find_element_xpath(self, f"//div[{i + 1}]/div/mat-form-field/div/div/div/mat-select/div/div[2]").click()
        wait_condition_located(self, self, "//mat-option/span[contains(., 'Scale')]", time=15)
        find_element_xpath(self, "//mat-option/span[contains(., 'Scale')]").click()

        wait_condition_located(self, self,
                               f"//div[{i + 1}]/div/mat-form-field/div/div/div/input[@formcontrolname='scale']",
                               time=15)
        find_element_xpath(self,
                           f"//div[{i + 1}]/div/mat-form-field/div/div/div/input[@formcontrolname='scale']").clear()
        find_element_xpath(self,
                           f"//div[{i + 1}]/div/mat-form-field/div/div/div/input[@formcontrolname='scale']").send_keys(
            str(scale[i]))
        time.sleep(1)
    # Parameters:
    if as_one_object:
        expand_upload_parameters(self).click()
        time.sleep(1)
        import_as_one_object_switch(self).click()
        time.sleep(1)
    if increase_rendering_efficiency:
        expand_upload_parameters(self).click()
        time.sleep(1)
        increase_rendering_efficiency_switch(self).click()
        time.sleep(1)
    # Actions on conflicting POI objects:
    if update_poi_position_and_poi_info:
        choose_actions_on_conflicting_poi_objects(self, update_position_and_poi_information=True)
    if delete_poi_objects_and_recreate_them:
        choose_actions_on_conflicting_poi_objects(self, delete_poi_objects_and_recreate_them=True)
    # Click create button
    create_project_button(self).click()
    time.sleep(2)


def create_project_with_upload_from_dir_and_custom_scale(self, *file_names, project_name: str, path_to_the_dir: str,
                                                         scale: list, project_description=None,
                                                         as_one_object=None, increase_rendering_efficiency=None,
                                                         update_poi_position_and_poi_info=None,
                                                         delete_poi_objects_and_recreate_them=None):
    # Click add or create new project
    if is_element_present(self, By.XPATH, add_project_clickable):
        add_project_button(self).click()
    else:
        create_new_project_button(self).click()
    time.sleep(1)
    # Fill in name field
    project_name_textarea(self).send_keys(project_name)
    time.sleep(1)
    # Fill in description field
    if project_description:
        project_name_textarea(self).click()
        project_name_textarea(self).send_keys(project_description)
    time.sleep(1)
    # Upload files
    upload_file_from_dir(self, path_to_the_dir, *file_names)
    # Scale:
    for i in range(0, len(file_names)):
        find_element_xpath(self, f"//div[{i + 1}]/div/mat-form-field/div/div/div/mat-select/div/div[2]").click()
        wait_condition_located(self, self, "//mat-option/span[contains(., 'Scale')]", time=15)
        find_element_xpath(self, "//mat-option/span[contains(., 'Scale')]").click()

        wait_condition_located(self, self,
                               f"//div[{i + 1}]/div/mat-form-field/div/div/div/input[@formcontrolname='scale']",
                               time=15)
        find_element_xpath(self,
                           f"//div[{i + 1}]/div/mat-form-field/div/div/div/input[@formcontrolname='scale']").clear()
        find_element_xpath(self,
                           f"//div[{i + 1}]/div/mat-form-field/div/div/div/input[@formcontrolname='scale']").send_keys(
            str(scale[i]))
        time.sleep(1)
    # Parameters:
    if as_one_object:
        expand_upload_parameters(self).click()
        time.sleep(1)
        import_as_one_object_switch(self).click()
        time.sleep(1)
    if increase_rendering_efficiency:
        expand_upload_parameters(self).click()
        time.sleep(1)
        increase_rendering_efficiency_switch(self).click()
        time.sleep(1)
    # Actions on conflicting POI objects:
    if update_poi_position_and_poi_info:
        choose_actions_on_conflicting_poi_objects(self, update_position_and_poi_information=True)
    if delete_poi_objects_and_recreate_them:
        choose_actions_on_conflicting_poi_objects(self, delete_poi_objects_and_recreate_them=True)
    # Click create button
    create_project_button(self).click()
    time.sleep(2)


def create_project_with_upload_as_link_and_custom_units(self, *link_names, project_name: str,
                                                        project_description=None,
                                                        units: list,
                                                        as_one_object=None, increase_rendering_efficiency=None,
                                                        update_poi_position_and_poi_info=None,
                                                        delete_poi_objects_and_recreate_them=None):
    # Click add or create new project
    if is_element_present(self, By.XPATH, add_project_clickable):
        add_project_button(self).click()
    else:
        create_new_project_button(self).click()
    time.sleep(1)
    # Fill in name field
    project_name_textarea(self).send_keys(project_name)
    time.sleep(1)
    # Fill in description field
    if project_description:
        project_name_textarea(self).click()
        project_name_textarea(self).send_keys(project_description)
    time.sleep(1)
    # Upload files
    upload_file_as_link(self, *link_names)
    # Units:
    for i in range(1, len(units) + 1):
        unit = units[i - 1].split()
        unit = unit[0]
        find_element_xpath(self, f"//div[{i}]/div/mat-form-field/div/div/div/mat-select/div/div[2]").click()
        wait_condition_located(self, self, "//mat-option/span[contains(., 'Units')]", 15)
        find_element_xpath(self, "//mat-option/span[contains(., 'Units')]").click()
        time.sleep(1)
        find_element_xpath(self,
                           f"//div[{i}]/div/mat-form-field[2]/div/div/div/mat-select/div/div[2]").click()
        if unit == "m":
            find_element_xpath(self, "//mat-option[1]/span[contains(., 'm')]").click()
        if unit == "mm":
            find_element_xpath(self, "//mat-option[2]/span[contains(., 'mm')]").click()
        if unit == "ft":
            find_element_xpath(self, "//mat-option/span[contains(., 'ft')]").click()
        if unit == "in":
            find_element_xpath(self, "//mat-option/span[contains(., 'in')]").click()
        time.sleep(1)
    # Parameters:
    if as_one_object:
        expand_upload_parameters(self).click()
        time.sleep(1)
        import_as_one_object_switch(self).click()
        time.sleep(1)
    if increase_rendering_efficiency:
        expand_upload_parameters(self).click()
        time.sleep(1)
        increase_rendering_efficiency_switch(self).click()
        time.sleep(1)
    # Actions on conflicting POI objects:
    if update_poi_position_and_poi_info:
        choose_actions_on_conflicting_poi_objects(self, update_position_and_poi_information=True)
    if delete_poi_objects_and_recreate_them:
        choose_actions_on_conflicting_poi_objects(self, delete_poi_objects_and_recreate_them=True)
    # Click create button
    create_project_button(self).click()
    time.sleep(2)


def upload_files_from_dir_inside_the_project(self, *file_names,
                                             path_to_the_dir=None,
                                             as_one_object=None, increase_rendering_efficiency=None,
                                             update_poi_position_and_poi_info=None,
                                             delete_poi_objects_and_recreate_them=None):
    # Click "Upload"
    wait_condition_clickable(self, imports_upload_button_clickable, 15)
    imports_upload_button(self).click()
    time.sleep(1)
    # Upload files
    upload_file_from_dir(self, path_to_the_dir, *file_names)
    time.sleep(2)
    # Parameters:
    if as_one_object:
        expand_upload_parameters(self).click()
        time.sleep(1)
        import_as_one_object_switch(self).click()
        time.sleep(1)
    if increase_rendering_efficiency:
        expand_upload_parameters(self).click()
        time.sleep(1)
        increase_rendering_efficiency_switch(self).click()
        time.sleep(1)
    # Actions on conflicting POI objects:
    if update_poi_position_and_poi_info:
        choose_actions_on_conflicting_poi_objects(self, update_position_and_poi_information=True)
    if delete_poi_objects_and_recreate_them:
        choose_actions_on_conflicting_poi_objects(self, delete_poi_objects_and_recreate_them=True)
    # Click upload button
    upload_button(self).click()
    time.sleep(2)


def upload_files_as_link_inside_the_project(self, *link_names,
                                            as_one_object=None, increase_rendering_efficiency=None,
                                            update_poi_position_and_poi_info=None,
                                            delete_poi_objects_and_recreate_them=None):
    # Click "Upload"
    wait_condition_clickable(self, imports_upload_button_clickable, 15)
    imports_upload_button(self).click()
    time.sleep(1)
    # Upload files
    upload_file_as_link(self, *link_names)
    time.sleep(2)
    # Parameters:
    if as_one_object:
        expand_upload_parameters(self).click()
        time.sleep(1)
        import_as_one_object_switch(self).click()
        time.sleep(1)
    if increase_rendering_efficiency:
        expand_upload_parameters(self).click()
        time.sleep(1)
        increase_rendering_efficiency_switch(self).click()
        time.sleep(1)
    # Actions on conflicting POI objects:
    if update_poi_position_and_poi_info:
        choose_actions_on_conflicting_poi_objects(self, update_position_and_poi_information=True)
    if delete_poi_objects_and_recreate_them:
        choose_actions_on_conflicting_poi_objects(self, delete_poi_objects_and_recreate_them=True)
    # Click upload button
    upload_button(self).click()
    time.sleep(2)


def create_empty_project(self, project_name, project_description=None):
    # Click add or create new project
    if is_element_present(self, By.XPATH, add_project_clickable):
        add_project_button(self).click()
    else:
        create_new_project_button(self).click()
    time.sleep(1)
    # Fill in name field
    project_name_textarea(self).send_keys(project_name)
    time.sleep(1)
    # Fill in description field
    if project_description:
        project_name_textarea(self).click()
        project_name_textarea(self).send_keys(project_description)
    time.sleep(1)
    # Click create button
    create_project_button(self).click()
    time.sleep(2)


verificationErrors = []  # Define the verificationErrors attribute


def compare_values(self, message, current, reference):
    try:
        assert current == reference, message + f"{current} not equal {reference}"
        print(message + "OK")
    except AssertionError as e:
        print(message + "{0} not equal {1}".format(current, reference))
        verificationErrors.append(str(e))


def compare_perf(self, message, current, reference):
    try:
        self.assertTrue(current < reference)
        print(message + "OK {0} is less than {1}".format(current, reference))
    except AssertionError as e:
        print(message + "False {0} is more than {1}".format(current, reference))
        self.verificationErrors.append(str(e))


def check_cadmodel(self, number_of_models, message):
    driver = self.driver
    content_cad_models_open(self).click()
    time.sleep(1)
    cur_number_of_models = len(
        driver.find_elements_by_xpath(self, "//selection-list-item/mat-list-item/div/div[3]/span"))
    compare_values(self, message, cur_number_of_models, number_of_models)


def check_scan(self, number_of_scans, message):
    driver = self.driver
    content_lazer_scans_open(self).click()
    time.sleep(1)
    cur_number_of_scans = len(
        driver.find_elements_by_xpath(self, "//selection-list-item/mat-list-item/div/div[3]/span"))
    compare_values(self, message, cur_number_of_scans, number_of_scans)


def check_annotation(self, number_of_annotations, message):
    driver = self.driver
    content_annotations_open_button(self).click()
    time.sleep(1)
    cur_number_of_annotations = len(
        driver.find_elements_by_xpath(self, "//selection-list-item/mat-list-item/div/div[3]/span"))
    compare_values(self, message, cur_number_of_annotations, number_of_annotations)


def check_measurement(self, number_of_measurements, message):
    driver = self.driver
    content_measurements_open_button(self).click()
    time.sleep(1)
    cur_number_of_measurements = len(
        driver.find_elements_by_xpath(self, "//selection-list-item/mat-list-item/div/div[3]/p"))
    compare_values(self, message, cur_number_of_measurements, number_of_measurements)


def update_view(view_name, camera_pitch, camera_x, camera_y, camera_yaw, camera_z):
    query = "UPDATE saved_view SET camera_pitch = %s, camera_x = %s, camera_y = %s, camera_yaw = %s, camera_z = %s WHERE name = %s;"
    vars = camera_pitch, camera_x, camera_y, camera_yaw, camera_z, view_name
    # con = connect()
    # cur = con.cursor()
    # cur.execute(query, vars);
    # con.commit()
    # con.close()


def refresh_the_page(self):
    return self.driver.refresh()


def loading_time(self):
    start_time = time.time()
    while is_element_present(self, By.XPATH, "//indicator-lods/mat-progress-bar"):
        time.sleep(0.2)
    loading_time = time.time() - start_time - 3
    return loading_time


def wait_until_model_is_being_initialized(self):
    start_time = time.time()
    while is_element_present(self, By.XPATH, "//app-wait/div/p[text()='The model is being initialized...']"):
        time.sleep(0.1)
    wait_time = time.time() - start_time
    return wait_time


def wait_time(self):
    start_time = time.time()
    while is_element_present(self, By.XPATH,
                             "//app-wait/div/p[text()='Please wait a little bit. 3D data are prepared']"):
        # while is_element_present(self, By.XPATH, "//app-wait/div/p[text()='Please wait a little bit .
        # 3D data are prepared for editing']") :  #s3
        time.sleep(0.2)
    wait_time = time.time() - start_time
    return wait_time


def fill_text_field(text_field, input_string: str) -> None:
    """
    This function fills in the text field
    """
    for char in input_string:
        text_field.send_keys(char)


def is_exist(self, xpath: str, object_name: str, positive_message=None, negative_message=None) -> None:
    """
    This function checks for the presence of an object in the path
    """
    if is_element_present(self, By.XPATH, f"{xpath}[contains(., '{object_name}')]"):
        if positive_message:
            print(positive_message)
    else:
        raise TimeoutException(msg=negative_message)


def is_not_exist(self, xpath: str, object_name: str, positive_message=None, negative_message=None) -> None:
    """
    This function checks for the presence of an object in the path
    """
    if not is_element_present(self, By.XPATH, f"{xpath}[contains(., '{object_name}')]"):
        if positive_message:
            print(positive_message)
    else:
        raise TimeoutException(msg=negative_message)


def is_exist_2_objects(self, xpath: str, first_object_name: str, second_object_name=None,
                       positive_message=None, negative_message=None) -> None:
    """
    This function checks for the presence of an object in the path
    """
    if is_element_present(self, By.XPATH, f"{xpath}[contains(., '{first_object_name}')]") \
            and is_element_present(self, By.XPATH, f"{xpath}[contains(., '{second_object_name}')]"):
        if positive_message:
            print(positive_message)
    else:
        raise TimeoutException(msg=negative_message)


def extended_is_exist(self: object, xpath: str, *object_names: object, positive_message: object = None,
                      negative_message: object = None) -> None:
    """
    This function checks for the presence of an object in the path
    """
    if sum([is_element_present(self, By.XPATH, f"{xpath}[contains(., '{obj_name}')]")
            for obj_name in object_names]) == len(object_names):
        print(positive_message)
    else:
        raise TimeoutException(msg=negative_message)



def wait_until_job_is_over(self, action: str, first_job_name: str, second_job_name=None):
    """
    This function waits until job is over
    """
    i_limit = 0
    while is_element_present(self, By.XPATH,
                             f"//upload-tasks/mat-card/div/mat-table//span[contains(., '{first_job_name}')]") \
            or is_element_present(self, By.XPATH,
                                  f"//upload-tasks/mat-card/div/mat-table//span[contains(., '{second_job_name}')]"):
        time.sleep(1)
        i_limit += 1
        if is_element_present(self, By.XPATH, "//upload-tasks//span[contains(., 'No tasks to show')]"):
            break
        if is_element_present(self, By.XPATH, "//upload-tasks/mat-card/div/mat-table/mat-row/mat-cell/section/"
                                              "span[contains(., 'Failed')]"):
            raise TimeoutException(msg=f"\n {action} is failed!")
        if i_limit == 480:
            print(f" {action}  takes too long")
            break


def wait_until_long_job_is_over(self, action: str, first_job_name: str, second_job_name=None):
    """
    This function waits until job is over
    """
    i_limit = 0
    while is_element_present(self, By.XPATH,
                             f"//upload-tasks/mat-card/div/mat-table//span[contains(., '{first_job_name}')]") \
            or is_element_present(self, By.XPATH,
                                  f"//upload-tasks/mat-card/div/mat-table//span[contains(., '{second_job_name}')]"):
        time.sleep(1)
        i_limit += 1
        if is_element_present(self, By.XPATH, "//upload-tasks//span[contains(., 'Failed')]"):
            print(f"\n {action} is failed!")
            break
        if i_limit == 1000:
            print(f" {action}  takes too long")
            break


def cleanup(self, *p_name):
    """
    This function closes the project and deletes it via the project menu.
    """
    back_to_project_list(self).click()

    for proj_name in p_name:
        time.sleep(1)
        expand_menu_project(self, proj_name).click()
        time.sleep(1)
        remove_project_button(self).click()
        time.sleep(1)
        confirm_remove_button(self).click()
        time.sleep(1)


def cleanup_without_back_to_projects_list(self, *p_name):
    """
    This function deletes project via the project menu.
    """
    for proj_name in p_name:
        time.sleep(1)
        expand_menu_project(self, proj_name).click()
        time.sleep(1)
        remove_project_button(self).click()
        time.sleep(1)
        confirm_remove_button(self).click()
        time.sleep(1)


def wait_condition_located(self, element_name: str, time: int) -> None:
    """
    This function waits until the element appears in its place.
    """
    WebDriverWait(self.driver, time).until(EC.presence_of_element_located((By.XPATH, element_name)))


def wait_condition_clickable(self, element_name: str, time: int) -> None:
    """
    This function waits until the element becomes clickable.
    """
    WebDriverWait(self.driver, time).until(EC.element_to_be_clickable((By.XPATH, element_name)))


def wait_until_model_uploaded(self):
    counter_time = 0
    while is_element_present(self, By.XPATH, "//projects-item//upload-button"):
        time.sleep(1)
        counter_time += 1
        if counter_time == 800:
            raise TimeoutException(msg="Upload takes too long")


def wait_until_large_model_uploaded(self, time_limit: int):
    counter_time = 0
    while is_element_present(self, By.XPATH, "//projects-item//upload-button"):
        time.sleep(1)
        counter_time += 1
        if counter_time == time_limit:
            raise TimeoutException(msg="Upload takes too long")


def new_wait_until_job_is_over(self, action: str):
    wait_condition_located(self, self, element_name="//upload-tasks//span[contains(., 'No tasks to show')]",
                           time=200)
    if is_element_present(self, By.XPATH, "//upload-tasks//span[contains(., 'No tasks to show')]"):
        time.sleep(1)
        # Close Tasks
        wait_condition_clickable(self, close_tasks_button_clickable, 10)
        close_tasks_button(self).click()
        time.sleep(1)

    elif is_element_present(self, By.XPATH, "//upload-tasks/mat-card/div/mat-table/mat-row/mat-cell/div/span"
                                            "[contains(., 'Failed')]"):
        print(f"\n {action} is failed!")


def wait_until_job_is_completed(self, job_parameter: str):
    i_limit = 0
    while is_element_present(self, By.XPATH,
                             f"//upload-tasks//span[contains(., '{job_parameter}')]"):
        time.sleep(1)
        i_limit += 1
        if is_element_present(self, By.XPATH, f"//upload-tasks//span[contains(., '{job_parameter}')]/../.."
                                              f"//span[contains(., 'Completed')]"):
            print(f"\t\t{job_parameter} is Completed")
            break
        if is_element_present(self, By.XPATH, f"//upload-tasks//span[contains(., '{job_parameter}')]/../.."
                                              f"//span[contains(., 'Failed')]"):
            raise TimeoutException(msg=f"\n {job_parameter} is failed!")
        if i_limit == 480:
            print(f" {job_parameter}  takes too long")
            break


def export_to_file(self, file_name=None, checkbox_point_cloud=None, checkbox_models=None) -> None:
    """
    This function performs actions to export to project
    """
    driver = self.driver
    # Wait until export to project button appears
    wait_condition_located(self, export_to_file_button_clickable, 15)
    export_to_file_button(self).click()

    # Wait until export domain to a new project menu appears
    wait_condition_located(self, export_domain_cross_button_clickable, 15)

    # Disable checkbox
    if checkbox_point_cloud:
        driver.find_element_xpath(self, domain_checkbox_point_clouds).click()
        time.sleep(1)
    elif checkbox_models:
        driver.find_element_xpath(self, domain_checkbox_3d_models).click()
        time.sleep(1)
    if file_name:
        export_domain_cross_button(self).click()
        time.sleep(1)
        # Fill the text field
        etf_name_input(self).send_keys(file_name)

    # Wait until OK button will be available
    wait_condition_located(self, export_ok_button_clickable, 15)
    export_ok_button(self).click()
    time.sleep(1)


def export_to_project(self, new_project_name=None, wait_until_job_is_completed=None) -> None:
    """
    This function performs actions to export to project
    """
    # Wait until export to project button appears
    wait_condition_located(self, export_to_project_button_clickable, 15)
    export_to_project_button(self).click()
    time.sleep(1)
    if new_project_name:
        # Wait until export domain to a new project menu appears
        wait_condition_located(self, export_domain_cross_button_clickable, 15)
        export_domain_cross_button(self).click()
        time.sleep(1)
        etp_name_input(self).send_keys(new_project_name)

    # Wait until OK button will be available
    wait_condition_located(self, export_ok_button_clickable, 15)
    export_ok_button(self).click()
    time.sleep(1)

    if wait_until_job_is_completed:
        # Open Tasks
        wait_condition_clickable(self, tasks_button_clickable, 10)
        tasks_button(self).click()

        # Wait for creating domain
        wait_until_job_is_over(self, action='Export domain to project', first_job_name='Export domain to project')

        # Close Tasks
        wait_condition_clickable(self, close_tasks_button_clickable, 10)
        close_tasks_button(self).click()


def create_domain(self, domain_name: str, wait_until_job_is_completed=None) -> None:
    """
    This function performs actions to create a domain
    """
    # Click "Domain" button
    wait_condition_clickable(self, domain_button_clickable, 15)
    domain_button(self).click()

    # Click on the input bar
    wait_condition_clickable(self, domain_name_input_clickable, 15)
    domain_name_input(self).click()
    time.sleep(1)

    # Enter the domain name
    domain_name_input(self).send_keys(domain_name)

    # Click "Create" button
    wait_condition_located(self, domain_create_clickable, 15)
    domain_create_button(self).click()
    time.sleep(1)
    if wait_until_job_is_completed:
        # Open Tasks
        wait_condition_clickable(self, tasks_button_clickable, 10)
        tasks_button(self).click()

        # Wait for creating domain
        wait_until_job_is_over(self, action='Creating a domain', first_job_name='Modify domain trees')

        # Close Tasks
        wait_condition_clickable(self, close_tasks_button_clickable, 10)
        close_tasks_button(self).click()


def logout_from_account(self) -> None:
    """
    This function performs actions for logout from the account
    """
    time.sleep(1)
    user_menu_button(self).click()
    time.sleep(1)
    logout_button(self).click()
    time.sleep(3)


# Group of items:
def remove_item_of_group(self, cross_of_item: str, cancel=None, remove=None) -> None:
    # Click "cross" icon
    wait_condition_clickable(self, element_name=cross_of_item, time=15)
    find_element_xpath(self, xpath=cross_of_item).click()
    time.sleep(1)

    # Wait until Remove group item menu appears:
    wait_condition_located(self, remove_item_cancel_clickable, 15)

    if cancel:
        remove_item_cancel_button(self).click()
        time.sleep(1)
    if remove:
        remove_item_button(self).click()
        time.sleep(1)


def remove_group(self, cancel=None, delete=None) -> None:
    # Click 3 dots icon
    three_dots_icon_button(self).click()
    time.sleep(1)
    # Click "Delete"
    wait_condition_located(self, delete_group_clickable, 15)
    delete_group_button(self).click()
    time.sleep(1)

    # Wait until Remove group item menu appears:
    wait_condition_located(self, cancel_remove_clickable, 15)

    if cancel:
        cancel_remove_button(self).click()
        time.sleep(1)

    if delete:
        confirm_remove_button(self).click()
        time.sleep(1)


def add_item_to_group(self, group_name: str, discard=None, add=None) -> None:
    # Click "Groups"
    wait_condition_clickable(self, groups_button_clickable, 30)
    groups_button(self).click()
    time.sleep(1)
    wait_condition_clickable(self, groups_name_input_clickable, 15)

    # Expand combobox
    wait_condition_clickable(self, select_group_combobox, 15)
    select_group_combobox_button(self).click()
    time.sleep(1)
    find_element_xpath(self, group_name).click()
    time.sleep(1)

    if discard:
        # Click "Discard" button
        wait_condition_clickable(self, discard_groups_clickable, 15)
        discard_groups_button(self).click()
        time.sleep(1)

    if add:
        # Click "Save" button
        wait_condition_clickable(self, save_group_clickable, 15)
        save_group_button(self).click()
        time.sleep(1)


def create_group(self, group_name: str, discard=None, create=None) -> None:
    # Click "Groups"
    groups_button(self).click()
    time.sleep(1)
    # Click on the input bar
    wait_condition_clickable(self, groups_name_input_clickable, 15)
    groups_name_input(self).click()
    time.sleep(1)

    # Enter the group name
    groups_name_input(self).send_keys(group_name)

    if discard:
        # Click "Discard" button
        wait_condition_clickable(self, discard_groups_clickable, 15)
        discard_groups_button(self).click()
        time.sleep(1)

    if create:
        # Click "Create" button
        wait_condition_clickable(self, create_group_clickable, 15)
        create_group_button(self).click()
        time.sleep(1)


def upload_image(self, link_name):
    # Click on the Upload button
    imports_upload_button(self).click()
    # Wait until link button will be clickable
    wait_condition_clickable(self, link_button_clickable, 15)
    link_button(self).click()
    time.sleep(1)
    # Enter the link
    url_address_input(self).send_keys(f"{link_name}")
    # Wait until ok button will be clickable
    wait_condition_clickable(self, ok_url_button_clickable, 15)
    ok_url_button(self).click()
    time.sleep(1)
    # Click on upload button
    upload_button(self).click()
    time.sleep(2)

    # Open Tasks
    wait_condition_clickable(self, tasks_button_clickable, 10)
    tasks_button(self).click()
    # Wait for Image auto alignment job is over
    wait_until_job_is_over(self, action="Image auto alignment",
                           first_job_name="Image auto alignment")
    # Close Tasks
    wait_condition_clickable(self, close_tasks_button_clickable, 10)
    close_tasks_button(self).click()
    time.sleep(1)


def add_object_to_domain(self, domain_name: str, wait_until_job_is_completed=None):
    # Click "Domain" button
    wait_condition_clickable(self, element_name=domain_button_clickable, time=15)
    domain_button(self).click()
    time.sleep(1)

    # Click on the created domains list button:
    wait_condition_clickable(self, define_a_domain_list_clickable, 15)
    define_a_domain_list_button(self).click()
    time.sleep(1)
    # Select the domain
    wait_condition_located(self, element_name=f"/html/body//button[contains(., '{domain_name}')]", time=15)
    find_element_xpath(self, xpath=f"/html/body//button[contains(., '{domain_name}')]").click()
    time.sleep(1)

    # Click "Create" button
    wait_condition_located(self, domain_create_clickable, 15)
    domain_create_button(self).click()
    time.sleep(1)

    if wait_until_job_is_completed:
        # Open Tasks
        wait_condition_clickable(self, tasks_button_clickable, 10)
        tasks_button(self).click()
        time.sleep(1)
        # Wait for creating domain
        wait_until_job_is_over(self, action='Creating a domain', first_job_name='Modify domain trees')
        # Close Tasks
        wait_condition_clickable(self, close_tasks_button_clickable, 10)
        close_tasks_button(self).click()


# Sharing:
def switch_user(self, login: str, password: str):
    # Log out
    user_menu_button(self).click()
    # Wait until logout button will be clickable
    wait_condition_clickable(self, logout_button_clickable, 10)
    logout_button(self).click()
    time.sleep(3)
    if is_element_present(self, By.XPATH, local_clickable):
        local_button(self).click()
        time.sleep(1)
        wait_condition_clickable(self, forgot_password_clickable, 15)
    # Log in under second user
    # Enter username
    username_input(self).send_keys(os.getenv(login))
    time.sleep(1)
    # Enter password
    password_input(self).send_keys(os.getenv(password))
    time.sleep(1)
    # Wait until sign in button will be clickable
    wait_condition_clickable(self, sign_in_button_clickable, 15)
    sign_in_button(self).click()
    time.sleep(2)
    # Wait until profile button appears:
    wait_condition_clickable(self, element_name=user_menu_button_clickable, time=15)


def share_project(self, share_to_user: str, project_name: str, selected_role: str,
                  select_user=None, share_inside=None, share_outside=None):
    if share_inside:
        # Configuration button will be pressed inside the project
        wait_condition_clickable(self, configuration_button_clickable, 10)
        configuration_button(self).click()
        time.sleep(1)
    if share_outside:
        expand_menu_project(self, f"{project_name}").click()

    # Share button will be pressed inside the project
    wait_condition_clickable(self, share_project_button_clickable, 10)
    share_project_button(self).click()
    time.sleep(1)

    # Select the role
    # Wait until optional field will be clickable
    wait_condition_clickable(self, optional_field_clickable, 10)
    optional_field(self).click()
    # Wait until can view only button will be clickable
    wait_condition_clickable(self, f"//mat-option/span[contains(., '{selected_role}')]", 10)
    find_element_xpath(self, f"//mat-option/span[contains(., '{selected_role}')]").click()
    time.sleep(1)

    # Click for empty place
    point_setter(self, 200, 200)

    if select_user:
        # Click on search panel
        wait_condition_clickable(self, "//div/mat-form-field/div/div/div/mat-chip-list/div/input", 10)
        find_element_xpath(self,
                           "//autocomplete-clips/div/mat-form-field/div/div/div/mat-chip-list/div/input").click()
        # Choose user
        wait_condition_clickable(self, f"//body/div/div/div/div/mat-option/span[contains(., '{share_to_user}')]", 10)
        find_element_xpath(self,
                           f"//body/div/div/div/div/mat-option/span[contains(., '{share_to_user}')]").click()
        # Wait until add button will be clickable
        wait_condition_clickable(self, sharing_add_button_clickable, 10)
        sharing_add_button(self).click()
    # Wait until sharing done button will be clickable
    wait_condition_clickable(self, sharing_done_clickable, 10)
    sharing_done(self).click()
    time.sleep(1)
    if share_inside:
        # Go outside the project
        back_to_project_list(self).click()
        time.sleep(4)


def sharing_change_role(self, project_name: str, change_to: str):
    # Open edit menu
    expand_menu_project(self, f"{project_name}").click()

    # Share button will be pressed inside the project
    wait_condition_clickable(self, share_project_button_clickable, 10)
    share_project_button(self).click()
    time.sleep(1)
    # Expand list of roles
    find_element_xpath(self,
                       "//share/form/div/mat-dialog-content/div/mat-table//mat-select[@role='combobox']").click()
    wait_condition_clickable(self, f"//mat-option/span[contains(., '{change_to}')]", 10)
    # Disable checkbox with old role
    find_element_xpath(self, f"//mat-option[@aria-selected='true']").click()
    time.sleep(1)
    # Enable checkbox with new role
    find_element_xpath(self, f"//mat-option/span[contains(., '{change_to}')]").click()
    time.sleep(1)
    # Close role list by empty click
    point_setter(self, 50, 50)
    # Click "Apply"
    wait_condition_clickable(self, sharing_done_clickable, 10)
    sharing_done(self).click()


def share_domain(self, share_to_user: str, selected_role: str):
    # Click on the Share button
    right_panel_share_button(self).click()
    time.sleep(1)

    # Select the role
    # Wait until optional field will be clickable
    wait_condition_clickable(self, optional_field_clickable, 10)
    optional_field(self).click()
    # Wait until can view only button will be clickable
    wait_condition_clickable(self, f"//mat-option/span[contains(., '{selected_role}')]", 10)
    find_element_xpath(self, f"//mat-option/span[contains(., '{selected_role}')]").click()
    time.sleep(1)

    # Click for empty place
    point_setter(self, 200, 200)

    # Click on search panel
    wait_condition_clickable(self, "//div/mat-form-field/div/div/div/mat-chip-list/div/input", 10)
    find_element_xpath(self,
                       "//autocomplete-clips/div/mat-form-field/div/div/div/mat-chip-list/div/input").click()
    # Choose user
    wait_condition_clickable(self, f"//body/div/div/div/div/mat-option/span[contains(., '{share_to_user}')]", 10)
    find_element_xpath(self,
                       f"//body/div/div/div/div/mat-option/span[contains(., '{share_to_user}')]").click()
    # Wait until add button will be clickable
    wait_condition_clickable(self, sharing_add_button_clickable, 10)
    sharing_add_button(self).click()
    # Wait until sharing done button will be clickable
    wait_condition_clickable(self, sharing_done_clickable, 10)
    sharing_done(self).click()
    time.sleep(1)


def create_custom_poi_type(self, *attributes: str, poi_type_name=None, path_to_file=None, default_asrv_poi_icon=None,
                           custom_poi_icon=None) -> None:
    if not move_and_rotate_button(self).is_enabled():
        edit_mode_button(self).click()
        time.sleep(1)
    # Open Configuration
    wait_condition_clickable(self, configuration_button_clickable, 10)
    configuration_button(self).click()
    # Open Manage POI types
    wait_condition_clickable(self, manage_poi_types_button_clickable, 10)
    manage_poi_types_button(self).click()
    time.sleep(2)
    i = 0
    poi_types = self.find_elements_by_xpath(self, "//span[@class='model-title']")
    for item in poi_types:
        i += 1
        if item.text == poi_type_name:
            # Verify cancel:
            find_element_xpath(self,
                               f"//mat-list-item/span[contains(., '{poi_type_name}')]//mat-icon[@data-mat-icon-name='remove']").click()
            time.sleep(1)
            wait_condition_clickable(self, confirm_remove_clickable, 10)
            find_element_xpath(self, cancel_remove_clickable).click()
            time.sleep(1)
            # Verify delete:
            wait_condition_located(self,
                                   f"//configuration-entities//mat-list-item/span/div/span[contains(., '{poi_type_name}')]",
                                   30)
            find_element_xpath(self,
                               f"//mat-list-item/span[contains(., '{poi_type_name}')]//mat-icon[@data-mat-icon-name='remove']").click()
            time.sleep(1)
            wait_condition_clickable(self, confirm_remove_clickable, 10)
            confirm_remove_button(self).click()
            # log_parse(self)
            time.sleep(1)
            break
    time.sleep(1)
    # Open POI type
    wait_condition_clickable(self, add_poi_type_clickable, 10)
    add_poi_type_button(self).click()
    time.sleep(2)
    poi_type_name_input(self).click()
    poi_type_name_input(self).send_keys(f"{poi_type_name}")
    time.sleep(2)

    if custom_poi_icon:
        wait_condition_clickable(self, upload_icon_clickable, 15)
        upload_icon_button(self).click()
        upload_file(path_to_file=path_to_file, file_name=f"{custom_poi_icon}")
        if is_element_present(self, By.XPATH, f"//div/mat-select/div/div/span/mat-select-trigger/div/span"
                                              f"[contains(., '{custom_poi_icon}')]"):
            print(f"\tImage {custom_poi_icon} was uploaded to POI icon")
        else:
            raise TimeoutException(msg=f"\tImage {custom_poi_icon} was not uploaded to POI icon!")
    if default_asrv_poi_icon:
        wait_condition_clickable(self, choose_icon_clickable, 10)
        choose_icon_button(self).click()
        time.sleep(1)
        wait_condition_clickable(self, f"//mat-option[contains(., '{default_asrv_poi_icon}')]", 10)
        find_element_xpath(self, f"//mat-option[contains(., '{default_asrv_poi_icon}')]").click()
        time.sleep(1)
        if is_element_present(self, By.XPATH, f"//div/mat-select/div/div/span/mat-select-trigger/div/span"
                                              f"[contains(., '{default_asrv_poi_icon}')]"):
            print(f"\t{default_asrv_poi_icon} was selected to POI icon")
        else:
            raise TimeoutException(msg=f"\t{default_asrv_poi_icon} was not selected to POI icon!")

    for number in range(1, len(attributes) + 1):
        # Click Attribute
        wait_condition_clickable(self, add_attribute_clickable, 15)
        add_attribute_button(self).click()
        time.sleep(1)
        # Enter Attribute name
        wait_condition_clickable(self,
                                 f"//create-edit-poi-type//mat-row[{number}]//input[@name='attribute-name']", 10)
        find_element_xpath(self,
                           f"//create-edit-poi-type//mat-row[{number}]//input[@name='attribute-name']").click()
        find_element_xpath(self,
                           f"//create-edit-poi-type//mat-row[{number}]//input[@name='attribute-name']").send_keys(
            f"Attribute {number}")
        time.sleep(1)
        attribute = attributes[number - 1].split()
        if "Mandatory" in attribute:
            attribute = attribute[0]
            # Switch mandatory
            find_element_xpath(self,
                               f"//create-edit-poi-type//mat-row[{number}]//span[@class='mat-slide-toggle-bar']").click()
            time.sleep(1)
            print(f"\tAttribute {attribute} was switch to mandatory")
        else:
            attribute = attribute[0]
        # Click first type Attribute
        wait_condition_clickable(self,
                                 f"//create-edit-poi-type//mat-row[{number}]//mat-select[@role='combobox']", 10)
        find_element_xpath(self,
                           f"//create-edit-poi-type//mat-row[{number}]//mat-select[@role='combobox']").click()
        time.sleep(1)
        wait_condition_clickable(self, f"//div/mat-option/span[contains(., '{attribute}')]", 10)
        find_element_xpath(self, f"//div/mat-option/span[contains(., '{attribute}')]").click()
        time.sleep(1)
        print(f"\tAttribute {attribute} was added")
    # Click Save
    wait_condition_clickable(self, save_custom_poi_clickable, 10)
    save_custom_poi_button(self).click()
    time.sleep(1)
    # Close manage poi type menu:
    wait_condition_clickable(self, close_manage_poi_type_clickable, 10)
    close_manage_poi_type_button(self).click()
    time.sleep(1)


def change_poi_position(self, x_coordinate: float, y_coordinate: float, z_coordinate: float) -> None:
    driver = self.driver
    # Change x coordinate:
    find_element_xpath(self, x_coordinates_input).click()
    time.sleep(1)
    actions = ActionChains(driver)
    actions.key_down(Keys.CONTROL).send_keys("a").key_up(Keys.CONTROL).perform()
    time.sleep(1)
    actions.send_keys(Keys.BACKSPACE).send_keys(f"{x_coordinate}").perform()
    # Change y coordinate:
    find_element_xpath(self, y_coordinates_input).click()
    time.sleep(1)
    actions = ActionChains(driver)
    actions.key_down(Keys.CONTROL).send_keys("a").key_up(Keys.CONTROL).perform()
    time.sleep(1)
    actions.send_keys(Keys.BACKSPACE).send_keys(f"{y_coordinate}").perform()
    # Change z coordinate:
    find_element_xpath(self, z_coordinates_input).click()
    time.sleep(1)
    actions = ActionChains(driver)
    actions.key_down(Keys.CONTROL).send_keys("a").key_up(Keys.CONTROL).perform()
    time.sleep(1)
    actions.send_keys(Keys.BACKSPACE).send_keys(f"{z_coordinate}").perform()


def point_setter(self, *coordinate_list):
    """
    This function takes a list of coordinate (x, y)
    """

    i = 0
    j = 1

    while j < len(coordinate_list):
        print(f"X Offset: {coordinate_list[i]}, Y Offset: {coordinate_list[j]}")

        actions = ActionChains(self.driver)
        actions.move_to_element_with_offset(self.driver.find_element(By.TAG_NAME, 'body'), 0, 0)
        self.driver.execute_script("document.body.style.transform='scale(0.9)';")
        print(f"X Offset: {coordinate_list[i]}, Y Offset: {coordinate_list[j]}")
        actions.move_by_offset(coordinate_list[i], coordinate_list[j]).click().perform()
        time.sleep(1)
        i += 2
        j += 2


def move_by_manipulator_in_move_rotate(self, *number_on_the_numpad: str, x_arrow: int, y_arrow: int):
    driver = self.driver
    # Move the object
    wait_condition_clickable(self, move_and_rotate_button_clickable, 10)
    move_and_rotate_button(self).click()
    time.sleep(1)
    wait_condition_clickable(self, back_button_clickable, 60)

    # Click on the coordinate arrow
    actions = ActionChains(driver)
    actions.move_to_element_with_offset(driver.find_element(By.TAG_NAME, 'body'), 0, 0)
    actions.move_by_offset(x_arrow, y_arrow).click().perform()
    time.sleep(1)

    # Wait until manipulator buttons will appear
    wait_condition_located(self, manipulator_apply_clickable, 10)
    for number in number_on_the_numpad:
        find_element_xpath(self,
                           f"//numpad/mat-card/mat-card-content/div/div/div/div[contains(., '{number}')]").click()
        time.sleep(1)
    manipulator_apply_button(self).click()
    time.sleep(2)

    # Apply changes
    confirm_move_and_rotate_button(self).click()
    time.sleep(1)
    # Exit from move &rotate
    back_button(self).click()
    time.sleep(1)
    # Move cursor to empty space
    point_setter(self, 300, 300)


def copy_selected_object(self, object: str):
    # Move the object
    wait_condition_clickable(self, move_and_rotate_button_clickable, 10)
    move_and_rotate_button(self).click()
    time.sleep(1)
    wait_condition_clickable(self, back_button_clickable, 60)
    # Click copy
    move_and_rotate_copy_button(self).click()
    time.sleep(1)
    # Open Tasks
    wait_condition_clickable(self, tasks_button_clickable, 10)
    tasks_button(self).click()
    time.sleep(1)
    # Wait for creating domain
    wait_until_job_is_over(self, action=f'Copy {object}', first_job_name=f'Copy {object}')
    # Close Tasks
    wait_condition_clickable(self, close_tasks_button_clickable, 10)
    close_tasks_button(self).click()
    time.sleep(1)
    # Exit from move &rotate
    back_button(self).click()
    time.sleep(1)
    # Move cursor to empty space
    point_setter(self, 300, 300)


def move_by_manipulator_in_placement(self, *number_on_the_numpad: str, x_arrow: int, y_arrow: int):
    driver = self.driver
    # Click on the coordinate arrow
    actions = ActionChains(driver)
    actions.move_to_element_with_offset(driver.find_element(By.TAG_NAME, 'body'), 0, 0)
    actions.move_by_offset(x_arrow, y_arrow).click().perform()
    time.sleep(1)

    # Wait until manipulator buttons will appear
    wait_condition_located(self, manipulator_apply_clickable, 10)
    for number in number_on_the_numpad:
        find_element_xpath(self,
                           f"//numpad/mat-card/mat-card-content/div/div/div/div[contains(., '{number}')]").click()
        time.sleep(1)
    manipulator_apply_button(self).click()
    time.sleep(2)

    # Apply changes
    confirm_placement_button(self).click()
    time.sleep(1)
    # Exit from placement
    back_button(self).click()
    time.sleep(1)
    # Move cursor to empty space
    point_setter(self, 300, 300)


def move_by_manipulator_in_clash_detection(self, *number_on_the_numpad: str, x_arrow: int, y_arrow: int):
    driver = self.driver
    # Click on the coordinate arrow
    actions = ActionChains(driver)
    actions.move_to_element_with_offset(driver.find_element(By.TAG_NAME, 'body'), 0, 0)
    actions.move_by_offset(x_arrow, y_arrow).click().perform()
    time.sleep(1)

    # Wait until manipulator buttons will appear
    wait_condition_located(self, manipulator_apply_clickable, 10)
    for number in number_on_the_numpad:
        find_element_xpath(self,
                           f"//numpad/mat-card/mat-card-content/div/div/div/div[contains(., '{number}')]").click()
        time.sleep(1)
    manipulator_apply_button(self).click()
    time.sleep(2)

    # Apply changes
    confirm_move_and_rotate_button(self).click()
    time.sleep(1)
    # Exit from Clash Detection
    back_button(self).click()
    time.sleep(1)
    # Move cursor to empty space
    point_setter(self, 300, 300)


def move_by_click_and_hold(self, x_start: int, y_start: int, x_final: int, y_final: int,
                           with_move_and_rotate=None) -> None:
    driver = self.driver
    if with_move_and_rotate:
        # Open move and rotate
        move_and_rotate_button(self).click()
        # Wait until back button will be available
        time.sleep(1)
        wait_condition_clickable(self, back_button_clickable, 10)

    # Move actions:
    actions = ActionChains(driver)
    actions.move_to_element_with_offset(driver.find_element(By.TAG_NAME, 'body'), 0, 0)
    actions.move_by_offset(x_start, y_start).perform()
    time.sleep(1)
    actions = ActionChains(driver)
    actions.click_and_hold().perform()
    time.sleep(1)
    actions = ActionChains(driver)
    actions.move_to_element_with_offset(driver.find_element(By.TAG_NAME, 'body'), 0, 0)
    actions.move_by_offset(x_final, y_final).perform()
    time.sleep(1)
    actions = ActionChains(driver)
    actions.release().perform()
    time.sleep(5)
    if with_move_and_rotate:
        # Apply changes
        confirm_move_and_rotate_button(self).click()
        time.sleep(1)
        # Exit from move &rotate
        back_button(self).click()
        time.sleep(1)


def extract_region(self, *coordinate_list, confirm_extraction=None, cancel_extraction=None, region_name=None,
                   with_copy=None,
                   custom_name=None):
    # Start region selection
    extract_region_button(self).click()
    time.sleep(1)
    # Wait until start region selection button will appear
    wait_condition_clickable(self, start_selection_clickable, 10)
    if with_copy:
        copy_the_selected_area_button(self).click()
        time.sleep(1)
    elif custom_name:
        find_element_xpath(self, region_name_input).click()
        time.sleep(1)
        actions = ActionChains(self.driver)
        actions.key_down(Keys.CONTROL).send_keys("a").key_up(Keys.CONTROL).perform()
        time.sleep(1)
        actions.send_keys(Keys.BACKSPACE).send_keys(f"{custom_name}").perform()
        time.sleep(1)
    start_selection_button(self).click()
    time.sleep(1)
    # Set the points:
    point_setter(self, *coordinate_list)
    time.sleep(1)
    start_selection_button(self).click()
    time.sleep(5)
    # Wait until apply region button will appear
    wait_condition_clickable(self, confirm_region_clickable, 60)
    confirm_region_button(self).click()
    time.sleep(3)

    # Open Tasks
    wait_condition_clickable(self, tasks_button_clickable, 10)
    tasks_button(self).click()
    time.sleep(1)

    if confirm_extraction:
        # Wait for extraction
        wait_until_job_is_over(self, action="Region extraction",
                               first_job_name="Region extraction",
                               second_job_name="Update overall point cloud")

    elif cancel_extraction:
        # Wait for extraction
        if is_element_present(self, By.XPATH,
                              "//upload-tasks//span[contains(., 'Region extraction')]") \
                or is_element_present(self, By.XPATH,
                                      "//upload-tasks//span[contains(., 'Update overall point cloud')]"):
            # Cancel extraction
            if region_name:
                wait_condition_located(self, task_cancel_clickable, 15)
                task_cancel_button(self, job_parameter=f"{region_name}").click()
                time.sleep(1)
                # log_parse(self)
            if custom_name:
                wait_condition_located(self, task_cancel_clickable, 15)
                task_cancel_button(self, job_parameter=f"{custom_name}").click()
                time.sleep(1)
                # log_parse(self)
            print("\t\tRegion extraction was canceled")
        else:
            raise TimeoutException(msg="\t\tRegion extraction was not canceled!")

    # Close Tasks
    wait_condition_clickable(self, close_tasks_button_clickable, 10)
    close_tasks_button(self).click()
    time.sleep(1)
    cancel_region_button(self).click()
    time.sleep(1)


def create_an_annotation(self, *coordinate_list, description=None, text_from_clipboard=None,
                         color=None, copy_to_the_clipboard=None):
    # Add annotations
    annotation_button(self).click()
    time.sleep(1)
    # Set the points:
    point_setter(self, *coordinate_list)
    time.sleep(1)

    if not is_element_displayed(self, annotation_popup_available):
        point_setter(self, *coordinate_list)
    if description:
        ann_textarea(self).send_keys(f"{description}")
    if text_from_clipboard:
        ActionChains(self.driver).key_down(Keys.CONTROL).send_keys("v").key_up(Keys.CONTROL).perform()
    if copy_to_the_clipboard:
        annotation_popup_button(self).click()
        time.sleep(1)
        # Wait until annotation copy to clipboard menu will appear
        wait_condition_clickable(self, annotation_copy_available, 10)
        annotation_copy_button(self).click()
        time.sleep(1)
    time.sleep(1)
    back_button(self).click()
    time.sleep(1)


def delete_an_annotation(self, *coordinate_list, cancel=None, remove=None):
    # Set the points:
    point_setter(self, *coordinate_list)
    time.sleep(1)

    if not is_element_displayed(self, annotation_popup_available):
        point_setter(self, *coordinate_list)
    # Wait until annotation textarea will appear
    wait_condition_located(self, annotation_popup_available, 60)
    # Remove annotation
    annotation_popup_button(self).click()
    time.sleep(1)
    # Wait until content annotation popup menu remove button will appear
    wait_condition_clickable(self, annotation_remove_available, 10)
    annotation_remove(self).click()
    time.sleep(1)
    wait_condition_clickable(self, confirm_remove_clickable, 10)
    if cancel:
        cancel_remove_button(self).click()
        time.sleep(1)
        # Exit from annotation dialog:
        point_setter(self, 300, 300)
    if remove:
        confirm_remove_button(self).click()
        time.sleep(1)


def attach_file_to_the_annotation(self, *coordinate_list: int, path_to_file: str, file_name: str):
    # Set the points:
    point_setter(self, *coordinate_list)
    time.sleep(1)

    if not is_element_displayed(self, annotation_popup_available):
        point_setter(self, *coordinate_list)
    # Attach normal txt file
    annotation_popup_button(self).click()
    time.sleep(1)
    # Wait until annotation attach menu will appear
    wait_condition_clickable(self, annotation_attach_available, 10)
    annotation_attach_button(self).click()
    time.sleep(1)
    upload_file(path_to_file, file_name)
    time.sleep(1)
    find_element_xpath(self, "//div[@class='mat-tab-label-content']/span[contains(., 'FILES')]").click()
    time.sleep(1)


def create_a_measure(self, *coordinate_list, simple_distance=None, ortho_direction=None, parallel_direction=None,
                     along_x_direction=None, along_y_direction=None, along_z_direction=None, diameter=None, angle=None):
    # Wait until measure button will be available
    wait_condition_clickable(self, measure_button_clickable, 10)
    measure_button(self).click()
    time.sleep(1)
    if simple_distance:
        # Set the points:
        point_setter(self, *coordinate_list)

    if ortho_direction:
        # Add distance with orthogonal option
        orthogonal_direction_button(self).click()
        time.sleep(1)
        # Set the points:
        point_setter(self, *coordinate_list)

    if parallel_direction:
        # Add distance with parallel option
        parallel_direction_button(self).click()
        time.sleep(1)
        # Set the points:
        point_setter(self, *coordinate_list)

    if along_x_direction:
        # Add distance along X axis
        x_direction_button(self).click()
        time.sleep(1)
        # Set the points:
        point_setter(self, *coordinate_list)

    if along_y_direction:
        # Add distance along Y axis
        y_direction_button(self).click()
        time.sleep(1)
        # Set the points:
        point_setter(self, *coordinate_list)

    if along_z_direction:
        # Add distance along Z axis
        z_direction_button(self).click()
        time.sleep(1)
        # Set the points:
        point_setter(self, *coordinate_list)

    if diameter:
        # Choose diameter
        choose_measure_type(self, diameter=True)
        # Set the points:
        point_setter(self, *coordinate_list)

    if angle:
        # Choose angle
        choose_measure_type(self, angle=True)
        # Set the points:
        point_setter(self, *coordinate_list)

    # Wait until measure approve button will be available
    wait_condition_clickable(self, measure_approve_clickable, 10)
    measure_approve_button(self).click()
    time.sleep(1)
    back_button(self).click()
    time.sleep(1)


def create_chain_of_measure(self, *coordinate_list):
    driver = self.driver
    # Wait until measure button will be available
    wait_condition_clickable(self, measure_button_clickable, 10)
    measure_button(self).click()
    time.sleep(1)
    # Set the points:
    i = 0
    j = 1
    while j < len(coordinate_list):
        if is_element_present(self, By.XPATH, measure_chain_clickable):
            measure_chain_button(self).click()
        actions = ActionChains(driver)
        actions.move_to_element_with_offset(driver.find_element(By.TAG_NAME, 'body'), 0, 0)
        actions.move_by_offset(coordinate_list[i], coordinate_list[j]).click().perform()
        time.sleep(1)
        i += 2
        j += 2
    # Wait until measure approve button will be available
    wait_condition_clickable(self, measure_approve_clickable, 10)
    measure_approve_button(self).click()
    time.sleep(1)
    back_button(self).click()
    time.sleep(1)


def create_poi_without_attributes(self, *coordinate_list, poi_type_name: str, poi_name: str):
    # Click "Point of interest" button
    point_of_interest_button(self).click()
    time.sleep(1)
    # Wait until point of interest type window appears:
    wait_condition_located(self, poi_type_menu_clickable, 15)
    poi_type_menu_button(self).click()
    time.sleep(1)
    # Select the poi type
    wait_condition_located(self, element_name=f"//mat-option//span[contains(., '{poi_type_name}')]", time=30)
    find_element_xpath(self, xpath=f"//mat-option//span[contains(., '{poi_type_name}')]").click()
    time.sleep(1)
    # Add the POI on Point Cloud
    point_setter(self, *coordinate_list)
    # Wait until poi name text field appears
    wait_condition_located(self, poi_name_input_clickable, 30)
    # Fill the text field
    wait_condition_clickable(self, poi_name_input_clickable, 30)
    poi_name_input(self).send_keys(f"{poi_name}")
    # Click "Save" button
    wait_condition_clickable(self, save_edit_poi_button_clickable, 30)
    save_edit_poi_button(self).click()
    time.sleep(1)
    back_button(self).click()
    time.sleep(1)


def create_custom_view(self, x_coord: int, y_coord: int, custom_name=None):
    # Set first custom front view
    point_setter(self, x_coord, y_coord)
    # Wait until views capture button will be available
    wait_condition_clickable(self, view_button_clickable, 10)
    # Capture first view
    view_button(self).click()
    time.sleep(1)
    # Wait until views save button will be available
    if custom_name:
        create_view_name_input(self).clear()
        create_view_name_input(self).send_keys(f"{custom_name}")
        time.sleep(1)
    wait_condition_clickable(self, create_view_save_clickable, 10)
    create_view_save_button(self).click()
    # Wait until views list button will be available
    time.sleep(1)
    wait_condition_clickable(self, view_button_clickable, 10)


def cleanup_project_list(self):
    while is_element_present(self, By.XPATH,
                             "//projects//projects-item//button//mat-icon[@data-mat-icon-name='more-vertical']"):
        find_element_xpath(self,
                           "//projects//projects-item//button//mat-icon[@data-mat-icon-name='more-vertical']").click()
        wait_condition_clickable(self, remove_project_clickable, 15)
        remove_project_button(self).click()
        time.sleep(1)
        confirm_remove_button(self).click()
        time.sleep(1)


def image_resize(self, image_name: str, x_coord: int, y_coord: int, screenshot_number=None):
    """
    This function resizes the image to defined resolution. Generally called before image saving
    """
    if screenshot_number:
        im_name = f'{image_name}_{screenshot_number}'
    else:
        im_name = f'{image_name}'
    with Image.open(f'Test_report/output/{im_name}.png') as im:
        imageResized = im.resize((x_coord, y_coord))
        imageResized.save(f'Test_report/output/{im_name}_resized.png')


def close_downloads_bar(self):
    pyautogui.keyDown('ctrl')
    pyautogui.press('j')
    pyautogui.keyUp('ctrl')
    time.sleep(1)
    pyautogui.keyDown('ctrl')
    pyautogui.press('w')
    pyautogui.keyUp('ctrl')
    time.sleep(1)
    print("downloads bar is closed")


def set_building_pc_visualization_setting(self):
    # Open 'User preferences' menu
    wait_condition_clickable(self, user_menu_button_clickable, 10)
    user_menu_button(self).click()
    time.sleep(1)
    wait_condition_clickable(self, preferences_button_clickable, 10)
    preferences_button(self).click()
    time.sleep(1)
    # Open Display tab
    display_tab(self).click()
    # Close the window if Point Cloud visualization settings is already 'Building'
    if is_element_present(self, By.XPATH, f"//span[contains(., 'Building')]"):
        time.sleep(2)
        preferences_discard_button(self).click()
        time.sleep(2)
        # Set Point Cloud visualization settings to 'Building' and save changes
    else:
        choose_point_cloud_visualization_settings(self, building=True)
        preferences_save_button(self).click()
        time.sleep(2)
