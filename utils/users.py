from operator import itemgetter


# we can store test data in this module like users
import json
from pathlib import Path

list_user = [
    {"name": "invalid_user", "email": "invalidUser@test.com", "password": "qwert1235"},
    {"name": "valid_user", "email": "MrAA", "password": "Test1234!"},
    {"name": "Staff2", "email": "staff@test.com", "password": "qwert1235"},
    {"name": "Admin0", "email": "admin@test.com", "password": "qwert1234"},
    {"name": "Admin1", "email": "admin@test.com", "password": "qwert1234"},
    {"name": "Admin2", "email": "admin@test.com", "password": "qwert1234"},
]


def get_user(name):
    try:
        return next(user for user in list_user if user["name"] == name)
    except:
        print("\n     User %s is not defined, enter a valid user.\n" % name)

    # function for pytest users managment session against test threads active and non active user


def get_json_data(file_name):
    file_name_with_extension = f"{file_name}.json"

    json_path = Path(Path(__file__).absolute().parent.parent, "data", file_name_with_extension)
    with open(json_path) as json_file:
        data = json.load(json_file)
    return data
